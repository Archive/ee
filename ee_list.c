/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/
#include <ee_list.h>
#include "globals.h"
#include <ee_file.h>
#include <ee_image.h>
#include <ee_thumb.h>
#include "functions.h"

/* major hack */
#include "gnome-stock-menu-first.xpm"
#include "gnome-stock-menu-last.xpm"


static void
ee_list_cb_destroy(GtkWidget *widget, gpointer data);
static void
ee_list_cb_del(GtkWidget *widget, gpointer data);
static void
ee_list_cb_rem(GtkWidget *widget, gpointer data);
static void
ee_list_cb_select(GtkWidget *widget, gint num);
static guint
ee_list_cb_idle(gpointer data);
static void
ee_list_cb_gen_toggle(GtkWidget *widget, gpointer data);
static void
ee_list_cb_large_toggle(GtkWidget *widget, gpointer data);

typedef struct _listimage
{
  gchar *filename;
  gchar *filename_short;
  gchar  been_scanned;
} ListImage;

static void
ee_list_cb_destroy(GtkWidget *widget, gpointer data)
{
  gtk_widget_hide(widget);
  data = NULL;
}

static void
ee_list_cb_gen_toggle(GtkWidget *widget, gpointer data)
{
  gint idle;
  
  if (GTK_TOGGLE_BUTTON(widget)->active)
    {
      idle = gtk_idle_add_priority(19, (GtkFunction)ee_list_cb_idle, data);
      gtk_object_set_data(GTK_OBJECT(data), "idle", GINT_TO_POINTER (idle));
    }
  else
    {
      idle = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(data), "idle"));
      if (idle)
	{
	  gtk_idle_remove(idle);
	  gtk_object_set_data(GTK_OBJECT(data), "idle", GINT_TO_POINTER (0));
	}
    }
  gtk_object_set_data(GTK_OBJECT(data), "do_icons", 
		      GINT_TO_POINTER (GTK_TOGGLE_BUTTON(widget)->active));
  widget = NULL;
  data = NULL;
}

static void
ee_list_cb_large_toggle(GtkWidget *widget, gpointer data)
{
  GtkWidget *c;
  ListImage *li;
  gint i;
  gint idle;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(data), "list");
  
  for (i = 0; i < GTK_CLIST(c)->rows; i++)
    {
      li = (ListImage *)gtk_clist_get_row_data(GTK_CLIST(c), i);
      li->been_scanned = 0;
    }

  gtk_object_set_data(GTK_OBJECT(data), "large", 
		      GINT_TO_POINTER (GTK_TOGGLE_BUTTON(widget)->active));
  if (GTK_TOGGLE_BUTTON(widget)->active)
    gtk_clist_set_row_height(GTK_CLIST(c), 65);
  else  
    gtk_clist_set_row_height(GTK_CLIST(c), 25);
  idle = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(data), "idle"));
  if (!idle)
    {
      idle = gtk_idle_add_priority(19, (GtkFunction)ee_list_cb_idle, data);
      gtk_object_set_data(GTK_OBJECT(data), "idle", GINT_TO_POINTER (idle));
    }
  widget = NULL;
  data = NULL;
}

static void
ee_list_cb_del(GtkWidget *widget, gpointer data)
{
  gchar *file;
  
  file = ee_image_get_filename(image_display);
  if (file)
    ee_list_del_file(data, file);
  widget = NULL;
}

static void
ee_list_cb_rem(GtkWidget *widget, gpointer data)
{
  gchar *file;
  
  file = ee_image_get_filename(image_display);
  if (file)
    ee_list_del_filename(data, file);
  widget = NULL;
}

static void
ee_list_cb_select(GtkWidget *widget, gint num)
{
  ListImage *li;
  GdkImlibImage *im;
  
  li = (ListImage *)gtk_clist_get_row_data(GTK_CLIST(widget), num);
  if (!li) 
    return;

  if (gtk_clist_row_is_visible(GTK_CLIST(widget), num) != GTK_VISIBILITY_FULL)
    gtk_clist_moveto(GTK_CLIST(widget), num, 0, 0.5, 0.0);
  im = gdk_imlib_load_image(li->filename);
  ee_image_set_filename(image_display, li->filename);
  ee_image_set_image(image_display, im);
}

static guint
ee_list_cb_idle(gpointer data)
{
  GtkWidget *c;
  GdkImlibImage *im = NULL;
  gint i, w, h, large;
  GdkPixmap *pmap, *mask;
  ListImage *li;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(data), "list");
  large = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(data), "large"));
  
  for (i = 0; i < GTK_CLIST(c)->rows; i++)
    {
      li = (ListImage *)gtk_clist_get_row_data(GTK_CLIST(c), i);
      if (!li->been_scanned)
	{
	  if ((filesize(li->filename) > 0) && (isfile(li->filename)))
	    {
	      if (large)
		im = ee_thumb_find(li->filename, "icons", 256, 64);
	      else
		im = ee_thumb_find(li->filename, "minis", 256, 24);
	      if (im)
		{
		  w = im->rgb_width;
		  h = im->rgb_height;
		  gdk_imlib_render(im, w, h);
		  pmap = gdk_imlib_move_image(im);
		  mask = gdk_imlib_move_mask(im);
		  gtk_clist_set_pixmap(GTK_CLIST(c), i, 1, pmap, mask);
		  gdk_imlib_free_pixmap(pmap);
		  gdk_imlib_destroy_image(im);
		}
	    }
	  if (im)
	    li->been_scanned = 1;
	  else
	    {
	      g_free(li->filename);
	      g_free(li->filename_short);
	      g_free(li);
	      gtk_clist_remove(GTK_CLIST(c), i);
	    }
	  return TRUE;
	}
    }

  gtk_object_set_data(GTK_OBJECT(data), "idle", GINT_TO_POINTER (0));
  return FALSE;
}

void
ee_list_next(GtkWidget *widget, gpointer data)
{
  GtkWidget *c;
  gint row;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(data), "list");
  if (!c)
    return;
  if (GTK_CLIST(c)->selection)
    row = GPOINTER_TO_INT (GTK_CLIST(c)->selection->data) + 1;
  else
    row = 0;
  if (row < 0)
    row = 0;
  if (row >= GTK_CLIST(c)->rows)
    row = GTK_CLIST(c)->rows - 1;
  gtk_clist_select_row(GTK_CLIST(c), row, 0);
  widget = NULL;
}

void
ee_list_prev(GtkWidget *widget, gpointer data)
{
  GtkWidget *c;
  gint row;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(data), "list");
  if (!c)
    return;
  if (GTK_CLIST(c)->selection)
    row = GPOINTER_TO_INT (GTK_CLIST(c)->selection->data) - 1;
  else
    row = 0;
  if (row < 0)
    row = 0;
  if (row >= GTK_CLIST(c)->rows)
    row = GTK_CLIST(c)->rows - 1;
  gtk_clist_select_row(GTK_CLIST(c), row, 0);
  widget = NULL;
}

void
ee_list_start(GtkWidget *widget, gpointer data)
{
  GtkWidget *c;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(data), "list");
  if (!c)
    return;
  gtk_clist_select_row(GTK_CLIST(c), 0, 0);
  widget = NULL;
}

void
ee_list_end(GtkWidget *widget, gpointer data)
{
  GtkWidget *c;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(data), "list");
  if (!c)
    return;
  gtk_clist_select_row(GTK_CLIST(c), GTK_CLIST(c)->rows - 1, 0);
  widget = NULL;
}

void
ee_list_sort(GtkWidget *widget, gpointer data)
{
  GtkWidget *c;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(data), "list");
  if (!c)
    return;
/*  gtk_clist_sort(GTK_CLIST(c));*/
  widget = NULL;
}

void
ee_list_add_filename(GtkWidget *w, gchar *file)
{
  GtkWidget *c;
  gchar *txt[2] = 
    {
      NULL, ""
    };
  gint row;
  ListImage *li;
  gint idle_func;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "list");
  if (!c)
    return;
  li = g_malloc(sizeof(ListImage));
  li->filename = g_strdup(file);
  li->filename_short = fullfileof(file);
  li->been_scanned = 0;
  
  txt[0] = li->filename_short;
  row = gtk_clist_append(GTK_CLIST(c), txt); 
  gtk_clist_set_row_data(GTK_CLIST(c), row, li);
  idle_func = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(w), "idle"));
  if ((!idle_func) &&
      gtk_object_get_data(GTK_OBJECT(w), "do_icons"))
    {
      idle_func = gtk_idle_add_priority(19, (GtkFunction)ee_list_cb_idle, (gpointer)w);
      gtk_object_set_data(GTK_OBJECT(w), "idle", GINT_TO_POINTER (idle_func));
    }
}

void
ee_list_del_filename(GtkWidget *w, gchar *file)
{
  GtkWidget *c;
  ListImage *li;
  gint i;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "list");
  if (!c)
    return;

  for (i = 0; i < GTK_CLIST(c)->rows; i++)
    {
      li = (ListImage *)gtk_clist_get_row_data(GTK_CLIST(c), i);
      if (!strcmp(file, li->filename))
	{
	  g_free(li->filename);
	  g_free(li->filename_short);
	  g_free(li);
	  gtk_clist_remove(GTK_CLIST(c), i);
	  break;
	}
    }
}

static void
ee_list_del_yes(gint bnum, gpointer data) G_GNUC_UNUSED;
static void
ee_list_del_yes(gint bnum, gpointer data)
{
  gchar *file;

  file = (gchar *)data;
  printf("%i\n", bnum);
}

void
ee_list_del_file(GtkWidget *w, gchar *file)
{
  GtkWidget *d, *l;
  gint bnum;

  d = gnome_dialog_new(_("Do you wish to delete the file on disk?"),
		       GNOME_STOCK_BUTTON_OK,
		       GNOME_STOCK_BUTTON_CANCEL, 
		       NULL);
  l = gtk_label_new(_("Do you wish to delete the disk file:"));
  gtk_widget_show(l);
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), l, TRUE, TRUE, 5);
  l = gtk_label_new(file);
  gtk_widget_show(l);
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), l, TRUE, TRUE, 5);
  
  gtk_window_set_modal(GTK_WINDOW(d),TRUE);
  gtk_window_position(GTK_WINDOW(d), GTK_WIN_POS_MOUSE);
  bnum = gnome_dialog_run(GNOME_DIALOG(d));
  gtk_widget_destroy(d);
  if (bnum == 0)
    {
      rm(file);
      ee_list_del_filename(w, file);
    }
}

void
ee_list_show_filename(GtkWidget *w, gchar *file)
{
  GtkWidget *c;
  ListImage *li;
  gint i;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "list");
  if (!c)
    return;
  
  for (i = 0; i < GTK_CLIST(c)->rows; i++)
    {
      li = (ListImage *)gtk_clist_get_row_data(GTK_CLIST(c), i);
      if (!strcmp(file, li->filename))
	{
	  gtk_clist_select_row(GTK_CLIST(w), i, 0);
	  return;
	}
    }
}

/*
void
ee_list_load(GtkWidget *w, gchar *file)
{
}

void
ee_list_save(GtkWidget *w, gchar *file)
{
}
*/

void
ee_list_set_gen_thumbs(GtkWidget *w)
{
  GtkWidget *c;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "toggle_gen");
  if (!c)
    return;
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(c), 1);
  gtk_object_set_data(GTK_OBJECT(w), "large", GINT_TO_POINTER (1));
}

void
ee_list_unset_gen_thumbs(GtkWidget *w)
{
  GtkWidget *c;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "toggle_gen");
  if (!c)
    return;
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(c), 0);
  gtk_object_set_data(GTK_OBJECT(w), "large", GINT_TO_POINTER (0));
}

void
ee_list_set_large_thumbs(GtkWidget *w)
{
  GtkWidget *c;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "toggle_large");
  if (!c)
    return;
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(c), 1);
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "list");
  gtk_clist_set_row_height(GTK_CLIST(c), 65);
  gtk_object_set_data(GTK_OBJECT(w), "large", GINT_TO_POINTER (1));
}

void
ee_list_unset_large_thumbs(GtkWidget *w)
{
  GtkWidget *c;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "toggle_large");
  if (!c)
    return;
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(c), 0);
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "list");
  gtk_clist_set_row_height(GTK_CLIST(c), 25);
  gtk_object_set_data(GTK_OBJECT(w), "large", GINT_TO_POINTER (0));
}

void
ee_list_freeze(GtkWidget *w)
{
  GtkWidget *c;

  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "list");
  if (!c)
    return;
  
  gtk_clist_freeze(GTK_CLIST(c));
}

void
ee_list_thaw(GtkWidget *w)
{
  GtkWidget *c;

  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "list");
  if (!c)
    return;
  
  gtk_clist_thaw(GTK_CLIST(c));
}

GtkWidget *
ee_list_new(void)
{
  GtkWidget *w, *v, *c, *t, *icon, *sw;
  gint row_height;
  
  w = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_wmclass(GTK_WINDOW(w), "ee", "Image Viewer");
  gtk_window_set_title(GTK_WINDOW(w), _("Electric Eyes - Images"));
  gtk_container_border_width(GTK_CONTAINER(w), 2);
  gtk_window_set_policy(GTK_WINDOW(w), 0, 1, 0);
  gtk_widget_realize(w);
  
  gtk_signal_connect(GTK_OBJECT(w), "delete_event",
		     GTK_SIGNAL_FUNC(ee_list_cb_destroy), w);
  
  v = gtk_vbox_new(FALSE, 2);
  gtk_widget_show(v);
  gtk_container_add(GTK_CONTAINER(w), v);

  t = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_ICONS);
  gtk_widget_show(t);
  icon = gnome_stock_pixmap_widget(t, GNOME_STOCK_MENU_QUIT);
  gtk_toolbar_append_item(GTK_TOOLBAR(t),
			  _("Quit"),
			  _("Exit this program"),
			  _("Exit this program"),
			  icon,
			  func_exit,
			  w);
  icon = gnome_stock_pixmap_widget(t, GNOME_STOCK_MENU_OPEN);
  gtk_toolbar_append_item(GTK_TOOLBAR(t),
			  _("Open"),
			  _("Open a new file"),
			  _("Open a new file"),
			  icon,
			  func_open_image,
			  w);
  icon = gnome_pixmap_new_from_xpm_d(gnome_stock_menu_first_xpm);
  gtk_toolbar_append_item(GTK_TOOLBAR(t),
			  _("First"),
			  _("Select first item in list"),
			  _("Select first item in list"),
			  icon,
			  ee_list_start,
			  w);
  icon = gnome_stock_pixmap_widget(t, GNOME_STOCK_MENU_BACK);
  gtk_toolbar_append_item(GTK_TOOLBAR(t),
			  _("Previous"),
			  _("Select previous item in list"),
			  _("Select previous item in list"),
			  icon,
			  ee_list_prev,
			  w);
  icon = gnome_stock_pixmap_widget(t, GNOME_STOCK_MENU_FORWARD);
  gtk_toolbar_append_item(GTK_TOOLBAR(t),
			  _("Next"),
			  _("Select next item in list"),
			  _("Select next item in list"),
			  icon,
			  ee_list_next,
			  w);
  icon = gnome_pixmap_new_from_xpm_d(gnome_stock_menu_last_xpm);
  gtk_toolbar_append_item(GTK_TOOLBAR(t),
			  _("Last Item"),
			  _("Select last item in list"),
			  _("Select last item in list"),
			  icon,
			  ee_list_end,
			  w);
/*  
  icon = gnome_stock_pixmap_widget(t, GNOME_STOCK_MENU_REFRESH);
  gtk_toolbar_append_item(GTK_TOOLBAR(t),
			  _("Sort"),
			  _("Sort list alphabetically"),
			  _("Sort list alphabetically"),
			  icon,
			  ee_list_sort,
			  w);
 */
  icon = gnome_stock_pixmap_widget(t, GNOME_STOCK_MENU_CLOSE);
  gtk_toolbar_append_item(GTK_TOOLBAR(t),
			  _("Remove"),
			  _("Remove filename from list"),
			  _("Remove filename from list"),
			  icon,
			  ee_list_cb_rem,
			  w); 
  icon = gnome_stock_pixmap_widget(t, GNOME_STOCK_MENU_TRASH);
  gtk_toolbar_append_item(GTK_TOOLBAR(t),
			  _("Delete"),
			  _("Delete file on disk and remove from the list"),
			  _("Delete file on disk and remove from the list"),
			  icon,
			  ee_list_cb_del,
			  w);
  gtk_box_pack_start(GTK_BOX(v), t, FALSE, FALSE, 0);
  
  sw = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_show(sw);
  c = gtk_clist_new(2);
  gtk_widget_show(c);
  gtk_container_add(GTK_CONTAINER(sw), c);
  gtk_box_pack_start(GTK_BOX(v), sw, TRUE, TRUE, 0);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_widget_set_usize(c, 200, 240);
  gtk_clist_set_selection_mode(GTK_CLIST(c), GTK_SELECTION_BROWSE);
  gtk_clist_set_column_width(GTK_CLIST(c), 0, 120);
  gtk_clist_set_column_width(GTK_CLIST(c), 1, 48);
  gtk_widget_realize(c);
  row_height = (GTK_WIDGET(c)->style->font->ascent +
		GTK_WIDGET(c)->style->font->descent + 1);
  if (row_height < 25)
    gtk_clist_set_row_height(GTK_CLIST(c), 25);
  gtk_signal_connect(GTK_OBJECT(c), "select_row", 
		     GTK_SIGNAL_FUNC(ee_list_cb_select), NULL);
  gtk_object_set_data(GTK_OBJECT(w), "list", c);
  
  c = gtk_check_button_new_with_label(_("Use Large Thumbnails"));
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(c), 0);
  gtk_widget_show(c);
  gtk_signal_connect(GTK_OBJECT(c), "clicked",
		       GTK_SIGNAL_FUNC(ee_list_cb_large_toggle),
		       w);
  gtk_object_set_data(GTK_OBJECT(w), "toggle_large", c);
  gtk_box_pack_start(GTK_BOX(v), c, FALSE, FALSE, 0);
  c = gtk_check_button_new_with_label(_("Generate Thumbnails"));
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(c), 1);
  gtk_widget_show(c);
  gtk_signal_connect(GTK_OBJECT(c), "clicked",
		       GTK_SIGNAL_FUNC(ee_list_cb_gen_toggle),
		       w);
  gtk_object_set_data(GTK_OBJECT(w), "toggle_gen", c);
  gtk_box_pack_start(GTK_BOX(v), c, FALSE, FALSE, 0);
  
  gtk_object_set_data(GTK_OBJECT(c), "widget", w);
  gtk_object_set_data(GTK_OBJECT(w), "idle", GINT_TO_POINTER (0));
  gtk_object_set_data(GTK_OBJECT(w), "widget", w);
  gtk_object_set_data(GTK_OBJECT(w), "do_icons", GINT_TO_POINTER (1));
  gtk_object_set_data(GTK_OBJECT(w), "large", GINT_TO_POINTER (0));
  return w;
}
