# ee ko.po
# Copyright (C) 1998 Free Software Foundation, Inc.
# Changwoo Ryu <cwryu@adam.kaist.ac.kr>, 1998
#
msgid ""
msgstr ""
"Project-Id-Version: ee 0.3\n"
"POT-Creation-Date: 2000-07-17 17:43-0400\n"
"PO-Revision-Date: 1998-09-18 04:18:02+0900\n"
"Last-Translator: Sung-Hyun Nam <namsh@lgic.co.kr>\n"
"Language-Team: Korean <ko@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=euc-kr\n"
"Content-Transfer-Encoding: 8bit\n"

#: ee_edit.c:1100
msgid "Electric Eyes - Edit Controls"
msgstr "Electric Eyes - Edit Controls"

#: ee_edit.c:1111
#, fuzzy
msgid "Color Settings"
msgstr "색상 설정"

#: ee_edit.c:1217
msgid "Gray Controls"
msgstr "회색 조정"

#: ee_edit.c:1245
msgid "Red Controls"
msgstr "적색 조정"

#: ee_edit.c:1273
msgid "Green Controls"
msgstr "녹색 조정"

#: ee_edit.c:1301
msgid "Blue Controls"
msgstr "청색 조정"

#: ee_edit.c:1330
#, fuzzy
msgid "Color Modifications:"
msgstr "색상 수정:"

#: ee_edit.c:1338
msgid "Apply"
msgstr "적용"

#: ee_edit.c:1339 ee_edit.c:1340
msgid "Apply the current color changes to the main image"
msgstr "현재 색상 변경을 그림에 적용하기"

#: ee_edit.c:1342
msgid "Keep"
msgstr "유지"

#: ee_edit.c:1343 ee_edit.c:1344
msgid "Apply the current color changes to the image data, and reset"
msgstr "현재 색상 변경을 이미지 데이타에 적용하고 리셋하기"

#: ee_edit.c:1346
msgid "Reset"
msgstr "리셋"

#: ee_edit.c:1347 ee_edit.c:1348
msgid "Reset all the color changes to normal values"
msgstr "모든 색상 변경을 보통 값으로 리셋하기"

#: ee_edit.c:1350
msgid "Always Apply"
msgstr "항상 적용"

#: ee_edit.c:1356 ee_edit.c:1357
msgid "Always apply any changes to the main image"
msgstr "모든 변경사항을 그림에 적용하기"

#: ee_edit.c:1359
msgid "Geometry Settings"
msgstr "위치/크기 설정"

#: ee_edit.c:1371
msgid "Image Size:"
msgstr "그림 크기:"

#: ee_edit.c:1376
msgid "Crop:"
msgstr "잘라내기:"

#: ee_edit.c:1390 ee_edit.c:1391
msgid "Halve the size of the image"
msgstr "그림의 크기를 반으로 줄임"

#: ee_edit.c:1395 ee_edit.c:1396
#, c-format
msgid "Reduce the image size by 10%"
msgstr "그림 크기를 10% 줄임"

#: ee_edit.c:1399
msgid "Normal"
msgstr "보통"

#: ee_edit.c:1400 ee_edit.c:1401
msgid "Set the image size to its normal size"
msgstr "그림 크기를 원래 크기로 맞춤"

#: ee_edit.c:1405 ee_edit.c:1406
#, c-format
msgid "Increase the image size by 10%"
msgstr "그림 크기를 10% 늘임"

#: ee_edit.c:1410 ee_edit.c:1411
msgid "Double the size of the image"
msgstr "그림 크기를 두배로 늘림"

#: ee_edit.c:1414
msgid "Max Aspect"
msgstr "최대 가로세로"

#: ee_edit.c:1415 ee_edit.c:1416
msgid "Maximize image size retaining aspect"
msgstr "가로세로 비율을 유지하면서 그림 최대화"

#: ee_edit.c:1419
msgid "Max"
msgstr "최대"

#: ee_edit.c:1420 ee_edit.c:1421
msgid "Maximize image size"
msgstr "그림 크기 최대화"

#: ee_edit.c:1428
msgid "Set Size"
msgstr "크기 결정"

#: ee_edit.c:1429 ee_edit.c:1430
msgid "Set a custom size for the image"
msgstr "그림 크기를 임의로 결정하기"

#: ee_edit.c:1433
msgid "Apply Size"
msgstr "크기 적용"

#: ee_edit.c:1434 ee_edit.c:1435
msgid "Apply the current view size to the image"
msgstr "현재 결정한 크기대로 그림 크기 맞추기"

#: ee_edit.c:1439 ee_edit.c:1440
msgid "Flip image horizontally"
msgstr "수평으로 그림 뒤집기"

#: ee_edit.c:1444 ee_edit.c:1445
msgid "Flip image vertically"
msgstr "수직으로 그림 뒤집기"

#: ee_edit.c:1449 ee_edit.c:1450
msgid "Rotate image"
msgstr "그림 회전"

#: ee_edit.c:1453
msgid "Snapshot Screen Now"
msgstr "스크린 스냅샷"

#: ee_edit.c:1454 ee_edit.c:1455
msgid "Grab the contents of the entire screen now"
msgstr "전체 스크린의 내용을 캡처"

#: ee_edit.c:1458
msgid "Snapshot Custom"
msgstr "임의 스탭샷"

#: ee_edit.c:1459 ee_edit.c:1460
msgid "Select a custom window on the screen to snapshot"
msgstr "스크린상에 스냅샷할 윈도우 선택"

#: ee_edit.c:1466 ee_image.c:129 ee_image.c:447
#, c-format
msgid "Image Size: (%i x %i)"
msgstr "그림 크기: (%i x %i)"

#: ee_filesel.c:63
msgid "Select a file to save"
msgstr "저장할 파일 선택"

#: ee_filesel.c:76 ee_filesel.c:128 ee_filesel.c:570
msgid "Select a file to load"
msgstr "읽어올 파일 선택"

#: ee_filesel.c:103
msgid "Set Printout settings"
msgstr "인쇄 환경 설정"

#: ee_filesel.c:525
msgid "Yes"
msgstr "예"

#: ee_filesel.c:527
msgid "No"
msgstr "아니오"

#: ee_filesel.c:569
msgid "Image Viewer"
msgstr "그림 보기 프로그램"

#: ee_filesel.c:597
msgid "Saved Image Settings"
msgstr "그림 설정 저장"

#: ee_filesel.c:604
msgid "Print Command:"
msgstr "인쇄 명령:"

#: ee_filesel.c:617 ee_menu.c:28 ee_toolbar.c:57
msgid "Print"
msgstr "인쇄"

#: ee_filesel.c:623
msgid "Cancel"
msgstr "취소"

#: ee_filesel.c:635
msgid "Image Format:"
msgstr "그림 형식:"

#: ee_filesel.c:667
msgid "PS (PostScript)"
msgstr "PS (포스트스크립트)"

#: ee_filesel.c:695
msgid "Other (Automatic by extension)"
msgstr "그 외 (확장자에 따라 자동)"

#: ee_filesel.c:706
msgid "Quality Settings"
msgstr "품질 설정"

#: ee_filesel.c:718
msgid "Page Settings"
msgstr "페이지 설정"

#: ee_filesel.c:791
msgid "Color"
msgstr "색상"

#: ee_filesel.c:804
msgid "Scaling Factor:"
msgstr "확대/축소 비율:"

#: ee_filesel.c:833
msgid "Select a file"
msgstr "파일 선택"

#: ee_filesel.c:844
msgid "Load all files in directory"
msgstr "자료방 안의 모든 파일 읽어오기"

#: ee_filesel.c:858
msgid "Use Previews"
msgstr "미리보기 사용"

#: ee_filesel.c:868
msgid "Image Information"
msgstr "그림 정보"

#: ee_filesel.c:913
msgid "Filename:"
msgstr "파일이름:"

#: ee_filesel.c:919
msgid "Format:"
msgstr "형식:"

#: ee_filesel.c:925
msgid "Dimensions:"
msgstr "가로세로:"

#: ee_filesel.c:931
msgid "File Size:"
msgstr "파일 크기:"

#: ee_filesel.c:937
msgid "Has Transparency:"
msgstr "투명함 있음:"

#: ee_filesel.c:943
msgid "Last Modified Date:"
msgstr "마지막 수정된 시각:"

#: ee_grab.c:361
msgid "Grab Options"
msgstr "잡기 옵션"

#: ee_grab.c:373
msgid "Number of seconds to wait:"
msgstr "기다릴 초단위 시간:"

#: ee_grab.c:384
msgid "Hide EE windows"
msgstr "창 감추기"

#: ee_grab.c:390
msgid "Selected window hierachy depth:"
msgstr "선택된 창 계층 깊이:"

#: ee_image.c:384 ee_image.c:538 ee_image.c:875
#, c-format
msgid "Crop: (%i, %i) (%i x %i)"
msgstr "다듬기: (%i, %i) (%i x %i)"

#: ee_image.c:916
msgid "Electric Eyes"
msgstr "Electric Eyes"

#: ee_list.c:347
msgid "Do you wish to delete the file on disk?"
msgstr "파일을 지우고 싶으세요?"

#: ee_list.c:351
msgid "Do you wish to delete the disk file:"
msgstr "디스크 파일을 지우고 싶으세요:"

#: ee_list.c:487
msgid "Electric Eyes - Images"
msgstr "Electric Eyes - 그림"

#: ee_list.c:503 ee_menu.c:55 ee_menu.c:126
msgid "Quit"
msgstr "끝내기"

#: ee_list.c:504 ee_list.c:505 ee_menu.c:56 ee_menu.c:127
msgid "Exit this program"
msgstr "이 프로그램을 끝냅니다"

#: ee_list.c:511 ee_menu.c:15 ee_toolbar.c:33
msgid "Open"
msgstr "열기"

#: ee_list.c:512 ee_list.c:513 ee_toolbar.c:34 ee_toolbar.c:35
msgid "Open a new file"
msgstr "새로운 파일 열기"

#: ee_list.c:519 ee_toolbar.c:65
msgid "First"
msgstr "첫번째"

#: ee_list.c:520 ee_list.c:521
msgid "Select first item in list"
msgstr "목록에서 첫번째 그림 선택"

#: ee_list.c:527 ee_toolbar.c:73
msgid "Previous"
msgstr "앞"

#: ee_list.c:528 ee_list.c:529
msgid "Select previous item in list"
msgstr "목록에서 앞 그림 선택"

#: ee_list.c:535 ee_toolbar.c:81
msgid "Next"
msgstr "다음"

#: ee_list.c:536 ee_list.c:537
msgid "Select next item in list"
msgstr "목록에서 다음 그림 선택"

#: ee_list.c:543
msgid "Last Item"
msgstr "마지막 그림"

#: ee_list.c:544 ee_list.c:545
msgid "Select last item in list"
msgstr "리스트의 마지막 그림 선택"

#: ee_list.c:561
msgid "Remove"
msgstr "빼기"

#: ee_list.c:562 ee_list.c:563
msgid "Remove filename from list"
msgstr "목록에서 파일이름 빼기"

#: ee_list.c:569
msgid "Delete"
msgstr "지우기"

#: ee_list.c:570 ee_list.c:571
msgid "Delete file on disk and remove from the list"
msgstr "디스크의 파일을 지우고 목록에서 빼기"

#: ee_list.c:598
msgid "Use Large Thumbnails"
msgstr "큰 썸네일 사용"

#: ee_list.c:606
msgid "Generate Thumbnails"
msgstr "썸네일 생성"

#: ee_menu.c:16
msgid "Open up a new image file"
msgstr "새로운 그림 파일을 열기"

#: ee_menu.c:19 ee_toolbar.c:41
msgid "Save"
msgstr "저장"

#: ee_menu.c:20
msgid "Save this image"
msgstr "이 그림을 저장함"

#: ee_menu.c:23 ee_toolbar.c:49
msgid "Save As"
msgstr "다른 이름으로 저장"

#: ee_menu.c:24
msgid "Save this image as a new file"
msgstr "이 이미지를 새로운 파일로 저장함"

#: ee_menu.c:29
msgid "Print this Image"
msgstr "그림을 인쇄"

#: ee_menu.c:33
msgid "Last Image"
msgstr "마지막 그림"

#: ee_menu.c:34
msgid "Display the last image in the image list"
msgstr "그림 목록에 있는 마지막 그림을 표시"

#: ee_menu.c:37
msgid "Next Image"
msgstr "다음 그림"

#: ee_menu.c:38
msgid "Display the next image in the image list"
msgstr "그림 목록에 있는 다음 그림을 표시"

#: ee_menu.c:41
msgid "Previous Image"
msgstr "앞 그림"

#: ee_menu.c:42
msgid "Display the previous image in the image list"
msgstr "그림 목록에 있는 앞 그림을 표시"

#: ee_menu.c:45
msgid "First Image"
msgstr "첫번째 그림"

#: ee_menu.c:46
msgid "Display the first image in the image list"
msgstr "그림 목록에 있는 첫번째 그림을 표시"

#: ee_menu.c:50 ee_menu.c:51
msgid "Set as Desktop Background"
msgstr "데스크탑 배경으로 지정"

#: ee_menu.c:64
msgid "Top"
msgstr "꼭대기"

#: ee_menu.c:65
msgid "Add Toolbar To Top"
msgstr "꼭대기에 툴바 추가"

#: ee_menu.c:68
msgid "Bottom"
msgstr "밑바닥"

#: ee_menu.c:69
msgid "Add Toolbar To Bottom"
msgstr "밑바닥에 툴바 추가"

#: ee_menu.c:72
msgid "Left"
msgstr "왼쪽"

#: ee_menu.c:73
msgid "Add Toolbar To Left"
msgstr "왼쪽에 툴바 추가"

#: ee_menu.c:76
msgid "Right"
msgstr "오른쪽"

#: ee_menu.c:77
msgid "Add Toolbar To Right"
msgstr "오른쪽에 툴바 추가"

#: ee_menu.c:81
msgid "Hide"
msgstr "숨기기"

#: ee_menu.c:82
msgid "Hide the Toolbar"
msgstr "툴바 숨기기"

#: ee_menu.c:90
msgid "Show/Hide Edit Window"
msgstr "편집 윈도우 보이기/감추기"

#: ee_menu.c:91
msgid "Toggle Edit Window visability"
msgstr "편집 창 보이기 토글"

#: ee_menu.c:94
msgid "Show/Hide List Window"
msgstr "목록 창 보이기/감추기"

#: ee_menu.c:95
msgid "Toggle List Window visability"
msgstr "목록 창 보이기 토글"

#: ee_menu.c:98
msgid "Toggle Scrolled View"
msgstr "스크롤 뷰 토글"

#: ee_menu.c:99
msgid "Toggle Scrollbars in a scrolled view"
msgstr "스크롤 바 토글"

#: ee_menu.c:102
msgid "Tool Bar"
msgstr "툴바"

#: ee_menu.c:109
msgid "File"
msgstr "파일"

#: ee_menu.c:111
msgid "View"
msgstr "보기"

#: ee_menu.c:114
msgid "Crop Selection"
msgstr "다듬기 선택"

#: ee_menu.c:115
msgid "Crop the current crop selection"
msgstr "현재의 선택을 다듬기"

#: ee_menu.c:118
msgid "Save Settings"
msgstr "설정 저장"

#: ee_menu.c:119
msgid "Save the current settings"
msgstr "현재 설정 저장"

#: ee_toolbar.c:25
msgid "Exit"
msgstr "끝내기"

#: ee_toolbar.c:26 ee_toolbar.c:27
msgid "Exit Electric Eyes"
msgstr "일렉트릭 아이즈 종료"

#: ee_toolbar.c:42 ee_toolbar.c:43
msgid "Save as original filename"
msgstr "원 파일이름으로 저장"

#: ee_toolbar.c:50 ee_toolbar.c:51
msgid "Save as new file"
msgstr "새로운 파일로 저장"

#: ee_toolbar.c:58 ee_toolbar.c:59
msgid "Print image"
msgstr "그림 인쇄"

#: ee_toolbar.c:66 ee_toolbar.c:67
msgid "View the first image in the list"
msgstr "목록에서 첫번째 그림을 표시"

#: ee_toolbar.c:74 ee_toolbar.c:75
msgid "View the previous image in the list"
msgstr "목록에서 앞 그림을 표시"

#: ee_toolbar.c:82 ee_toolbar.c:83
msgid "View the next image in the list"
msgstr "목록에서 다음 그림을 표시"

#: ee_toolbar.c:89
msgid "Last"
msgstr "마지막"

#: ee_toolbar.c:90 ee_toolbar.c:91
msgid "View the last image in the list"
msgstr "목록에서 마지막 그림을 표시"

#: ee_toolbar.c:97
msgid "Crop"
msgstr "잘라내기"

#: ee_toolbar.c:98 ee_toolbar.c:99
msgid "Crop the current selection"
msgstr "현재의 선택한 부분을 잘라내기"

#: functions.c:652
msgid "Select a window"
msgstr "창 선택"

#: functions.c:655
msgid "Please press OK then click on a window to grab"
msgstr "OK 버튼을 누른 후에 잡고 싶은 창을 클릭하세요"

#: main.c:25
msgid "Set root window image and leave"
msgstr "데스크탑 배경으로 지정하고 종료"
