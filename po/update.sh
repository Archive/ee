#!/bin/sh

xgettext --default-domain=ee --directory=.. \
  --add-comments --keyword=_ --keyword=N_ \
  --files-from=./POTFILES.in \
&& test ! -f ee.po \
   || ( rm -f ./ee.pot \
    && mv ee.po ./ee.pot )
