/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#include <config.h>
#include <gtk/gtk.h>
#include <gdk_imlib.h>
#include <gnome.h>

void
func_open_image(GtkWidget *widget, gpointer data);
void
func_save_image(GtkWidget *widget, gpointer data);
void
func_save_image_only(GtkWidget *widget, gpointer data);
void
func_hide_toolbar(GtkWidget *widget, gpointer data);
void
func_add_toolbar_top(GtkWidget *widget, gpointer data);
void
func_add_toolbar_bottom(GtkWidget *widget, gpointer data);
void
func_add_toolbar_left(GtkWidget *widget, gpointer data);
void
func_add_toolbar_right(GtkWidget *widget, gpointer data);
void
func_toggle_scrollable(GtkWidget *widget, gpointer data);
void
func_exit(GtkWidget *widget, gpointer data);
void
func_first(GtkWidget *widget, gpointer data);
void
func_prev(GtkWidget *widget, gpointer data);
void
func_next(GtkWidget *widget, gpointer data);
void
func_last(GtkWidget *widget, gpointer data);
void
func_config(GtkWidget *widget, gpointer data);
void
func_crop_image(GtkWidget *widget, gpointer data);
void
func_image_halve(GtkWidget *widget, gpointer data);
void
func_image_m10(GtkWidget *widget, gpointer data);
void
func_image_normal(GtkWidget *widget, gpointer data);
void
func_image_p10(GtkWidget *widget, gpointer data);
void
func_image_double(GtkWidget *widget, gpointer data);
void
func_image_grab(GtkWidget *widget, gpointer data);
void
func_show_edit(GtkWidget *widget, gpointer data);
void
func_show_list(GtkWidget *widget, gpointer data);
void
func_print_image(GtkWidget *widget, gpointer data);
void
func_flip_h(GtkWidget *widget, gpointer data);
void
func_flip_v(GtkWidget *widget, gpointer data);
void
func_flip_z(GtkWidget *widget, gpointer data);
void
func_appliy_size(GtkWidget *widget, gpointer data);
void
func_image_max_size(GtkWidget *widget, gpointer data);
void
func_image_max_aspect(GtkWidget *widget, gpointer data);
void
func_image_set_size(GtkWidget *widget, gpointer data);
void
func_window_grab(GtkWidget *widget, gpointer data);
void
func_set_root(GtkWidget *widget, gpointer data);

