/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/
#include "ee.h"

GtkWidget 
 *file_selector = NULL, 
 *image_display = NULL, 
 *main_menu = NULL,
 *image_toolbar = NULL,
 *padder_button = NULL,
 *image_list = NULL,
 *edit_window = NULL,
 *ee_app = NULL;
 
