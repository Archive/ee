
/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/* Scrollpane widget for GTK/GNOME
 * Copyright (C) 1998 Redhat Software Inc.
 * Author: Jonathan Blandford <jrb@redhat.com>
 */

#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include "gtkscrollpane.h"

#define SCROLLPANE_CLASS(w)  GTK_SCROLLPANE_CLASS (GTK_OBJECT (w)->klass)
#define SCROLLPANE_DEFAULT_SIZE 80;
#define EPSILON 0.00005
#define XTHICK 2
#define YTHICK 2

enum
  {
    MIDDLE_CLICKED,
    RIGHT_CLICKED,
    LAST_SIGNAL
  };

/* Forward declarations */
static void         gtk_scrollpane_init(GtkScrollpane * sp);
static void         gtk_scrollpane_class_init(GtkScrollpaneClass * klass);
static void         gtk_scrollpane_destroy(GtkObject * object);
static void         gtk_scrollpane_realize(GtkWidget * widget);
static void         gtk_scrollpane_unrealize(GtkWidget * widget);
static void         gtk_scrollpane_map(GtkWidget * widget);
static void         gtk_scrollpane_draw(GtkWidget * widget, GdkRectangle * area);
static void         gtk_scrollpane_draw_background(GtkScrollpane * sp);
static void         gtk_scrollpane_draw_trough(GtkScrollpane * sp);
static void         gtk_scrollpane_draw_slider(GtkScrollpane * sp);
static void         gtk_real_scrollpane_draw_trough(GtkScrollpane * sp);
static void         gtk_real_scrollpane_draw_slider(GtkScrollpane * sp);
static void         gtk_scrollpane_size_request(GtkWidget * widget, GtkRequisition * requisition);
static void         gtk_scrollpane_size_allocate(GtkWidget * widget, GtkAllocation * allocation);
static void         gtk_scrollpane_update_slider_size(GtkScrollpane * sp);
static void         gtk_scrollpane_style_set(GtkWidget * widget, GtkStyle * previous_style);
static void         gtk_scrollpane_adjustment_changed(GtkAdjustment * adjustment, gpointer data);
static void         gtk_scrollpane_value_adjustment_changed(GtkAdjustment * adjustment, gpointer data);
static gint         gtk_scrollpane_expose(GtkWidget * widget, GdkEventExpose * event);
static gint         gtk_scrollpane_button_press(GtkWidget * widget, GdkEventButton * event);
static gint         gtk_scrollpane_button_release(GtkWidget * widget, GdkEventButton * event);
static gint         gtk_scrollpane_key_press(GtkWidget * widget, GdkEventKey * event);
static gint         gtk_scrollpane_enter_notify(GtkWidget * widget, GdkEventCrossing * event);
static gint         gtk_scrollpane_leave_notify(GtkWidget * widget, GdkEventCrossing * event);
static gint         gtk_scrollpane_motion_notify(GtkWidget * widget, GdkEventMotion * event);
static gint         gtk_scrollpane_focus_in(GtkWidget * widget, GdkEventFocus * event);
static gint         gtk_scrollpane_focus_out(GtkWidget * widget, GdkEventFocus * event);

static GtkWidget   *parent_class = NULL;
static guint        scrollpane_signals[LAST_SIGNAL] =
{0};

static void
gtk_scrollpane_adjustment_value_changed(GtkAdjustment *a)
{
  /*  gtk_adjustment_value_changed(a);*/
    gtk_signal_emit_by_name(GTK_OBJECT(a), "value_changed");
}

/* Static, private functions */
guint
gtk_scrollpane_get_type(void)
{
  static guint        gtk_scrollpane_type = 0;

  if (!gtk_scrollpane_type)
    {
      GtkTypeInfo         gtk_scrollpane_info =
      {
	"GtkScrollpane",
	sizeof(GtkScrollpane),
	sizeof(GtkScrollpaneClass),
	(GtkClassInitFunc) gtk_scrollpane_class_init,
	(GtkObjectInitFunc) gtk_scrollpane_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL,
      };

      gtk_scrollpane_type = gtk_type_unique(gtk_widget_get_type(), &gtk_scrollpane_info);
    }
  return gtk_scrollpane_type;
}

static void
gtk_scrollpane_init(GtkScrollpane * sp)
{
  GtkWidget          *widget;

  widget = GTK_WIDGET(sp);
  sp->trough = NULL;
  sp->slider = NULL;

  sp->slider_height = 0;
  sp->slider_width = 0;

  sp->policy = GTK_UPDATE_CONTINUOUS;
  sp->Xadjustment = NULL;
  sp->Yadjustment = NULL;
  sp->moving = 0;
  sp->trough_width = SCROLLPANE_DEFAULT_SIZE;
  sp->trough_height = SCROLLPANE_DEFAULT_SIZE;

}

static void
gtk_scrollpane_class_init(GtkScrollpaneClass * klass)
{
  GtkObjectClass     *object_class;
  GtkWidgetClass     *widget_class;

  object_class = (GtkObjectClass *) klass;
  widget_class = (GtkWidgetClass *) klass;
  parent_class = gtk_type_class(gtk_widget_get_type());

  scrollpane_signals[MIDDLE_CLICKED] =
    gtk_signal_new("middle_clicked",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GtkScrollpaneClass, middle_clicked),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  scrollpane_signals[RIGHT_CLICKED] =
    gtk_signal_new("right_clicked",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GtkScrollpaneClass, right_clicked),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals(object_class, scrollpane_signals, LAST_SIGNAL);

  object_class->destroy = gtk_scrollpane_destroy;

  widget_class->realize = gtk_scrollpane_realize;
  widget_class->expose_event = gtk_scrollpane_expose;
  widget_class->size_request = gtk_scrollpane_size_request;
  widget_class->size_allocate = gtk_scrollpane_size_allocate;
  widget_class->button_press_event = gtk_scrollpane_button_press;
  widget_class->button_release_event = gtk_scrollpane_button_release;
  widget_class->motion_notify_event = gtk_scrollpane_motion_notify;
  widget_class->key_press_event = gtk_scrollpane_key_press;
  widget_class->enter_notify_event = gtk_scrollpane_enter_notify;
  widget_class->leave_notify_event = gtk_scrollpane_leave_notify;
  widget_class->focus_in_event = gtk_scrollpane_focus_in;
  widget_class->focus_out_event = gtk_scrollpane_focus_out;
  widget_class->style_set = gtk_scrollpane_style_set;

  klass->draw_background = NULL;
  klass->draw_trough = gtk_real_scrollpane_draw_trough;
  klass->draw_slider = gtk_real_scrollpane_draw_slider;
  klass->middle_clicked = NULL;
  klass->right_clicked = NULL;
}

static void
gtk_scrollpane_destroy(GtkObject * object)
{
  GtkScrollpane      *sp;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(object));

  sp = GTK_SCROLLPANE(object);

  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (*GTK_OBJECT_CLASS(parent_class)->destroy) (object);
}

static void
gtk_scrollpane_realize(GtkWidget * widget)
{
  GtkScrollpane      *sp;
  GdkWindowAttr       attributes;
  gint                attributes_mask;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(widget));

  sp = GTK_SCROLLPANE(widget);
  GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

  sp->min_slider_width = XTHICK + XTHICK + 1;
  sp->min_slider_height = YTHICK + YTHICK + 1;
  
  /* 
   * set up the widget
   */
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.event_mask = gtk_widget_get_events(widget) |
    GDK_EXPOSURE_MASK |
    GDK_BUTTON_PRESS_MASK |
    GDK_BUTTON_RELEASE_MASK |
    GDK_POINTER_MOTION_MASK |
    GDK_ENTER_NOTIFY_MASK |
    GDK_LEAVE_NOTIFY_MASK;

  attributes.visual = gtk_widget_get_visual(widget);
  attributes.colormap = gtk_widget_get_colormap(widget);

  attributes_mask = GDK_WA_X |
    GDK_WA_Y |
    GDK_WA_VISUAL |
    GDK_WA_COLORMAP;
  widget->window = gdk_window_new(widget->parent->window, &attributes, attributes_mask);

  sp->trough = widget->window;
  gdk_window_ref(sp->trough);

  sp->slider = gdk_window_new(sp->trough,
			      &attributes,
			      attributes_mask);

  gtk_scrollpane_update_slider_size(sp);
  widget->style = gtk_style_attach(widget->style, widget->window);

  gdk_window_set_user_data(sp->trough, widget);
  gtk_style_set_background(widget->style,
			   sp->trough,
			   GTK_STATE_ACTIVE);
  gdk_window_set_user_data(sp->slider, widget);
  gtk_style_set_background(widget->style,
			   sp->slider,
			   GTK_STATE_NORMAL);
  gdk_window_show(sp->slider);
}

static void
gtk_scrollpane_unrealize(GtkWidget * widget) G_GNUC_UNUSED;

static void
gtk_scrollpane_unrealize(GtkWidget * widget)
{
  GtkScrollpane      *sp;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(widget));

  sp = GTK_SCROLLPANE(widget);

  if (sp->slider)
    {
      gdk_window_set_user_data(sp->slider, NULL);
      gdk_window_destroy(sp->slider);
      sp->slider = NULL;
    }
  if (sp->trough)
    {
      gdk_window_set_user_data(sp->trough, NULL);
      gdk_window_destroy(sp->trough);
      sp->trough = NULL;
    }

  if (GTK_WIDGET_CLASS(parent_class)->unrealize)
    (*GTK_WIDGET_CLASS(parent_class)->unrealize) (widget);
}

static void
gtk_scrollpane_map(GtkWidget * widget) G_GNUC_UNUSED;

static void
gtk_scrollpane_map(GtkWidget * widget)
{
}

static void
gtk_scrollpane_draw(GtkWidget * widget, GdkRectangle * area) G_GNUC_UNUSED;
static void
gtk_scrollpane_draw(GtkWidget * widget, GdkRectangle * area)
{
  GtkScrollpane      *sp;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(widget));
  g_return_if_fail(area != NULL);

  if (GTK_WIDGET_DRAWABLE(widget))
    {
      sp = GTK_SCROLLPANE(widget);
      gtk_scrollpane_draw_background(sp);
      gtk_scrollpane_draw_trough(sp);
      gtk_scrollpane_draw_slider(sp);
    }
}

static void
gtk_scrollpane_draw_background(GtkScrollpane * sp)
{
  g_return_if_fail(sp != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(sp));

  if (sp->trough && SCROLLPANE_CLASS(sp)->draw_background)
    (*SCROLLPANE_CLASS(sp)->draw_background) (sp);
}

static void
gtk_scrollpane_draw_trough(GtkScrollpane * sp)
{
  g_return_if_fail(sp != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(sp));

  if (sp->trough && SCROLLPANE_CLASS(sp)->draw_trough)
    (*SCROLLPANE_CLASS(sp)->draw_trough) (sp);
}

static void
gtk_scrollpane_draw_slider(GtkScrollpane * sp)
{
  g_return_if_fail(sp != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(sp));

  if (sp->trough && SCROLLPANE_CLASS(sp)->draw_slider)
    (*SCROLLPANE_CLASS(sp)->draw_slider) (sp);
}

static void
gtk_real_scrollpane_draw_trough(GtkScrollpane * sp)
{
  g_return_if_fail(sp != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(sp));

  if (sp->trough)
    {
      gtk_draw_shadow(GTK_WIDGET(sp)->style,
		      sp->trough,
		      GTK_STATE_NORMAL,
		      GTK_SHADOW_IN,
		      0, 0, -1, -1);

      if (GTK_WIDGET_HAS_FOCUS(sp))
	gdk_draw_rectangle
	  (GTK_WIDGET(sp)->window,
	   GTK_WIDGET(sp)->style->black_gc,
	   FALSE, 0, 0,
	   GTK_WIDGET(sp)->allocation.width - 1,
	   GTK_WIDGET(sp)->allocation.height - 1);
      else if (sp->trough != GTK_WIDGET(sp)->window)
	gdk_draw_rectangle
	  (GTK_WIDGET(sp)->window,
	   GTK_WIDGET(sp)->style->bg_gc[GTK_STATE_NORMAL],
	   FALSE, 0, 0,
	   GTK_WIDGET(sp)->allocation.width - 1,
	   GTK_WIDGET(sp)->allocation.height - 1);
    }
}

static void
gtk_real_scrollpane_draw_slider(GtkScrollpane * sp)
{
  GtkStateType        state_type;

  g_return_if_fail(sp != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(sp));

  if (sp->slider)
    {
      state_type = GTK_STATE_NORMAL;

      gtk_style_set_background(GTK_WIDGET(sp)->style,
			       sp->slider,
			       state_type);
      gdk_window_clear(sp->slider);

      gtk_draw_shadow(GTK_WIDGET(sp)->style,
		      sp->slider, state_type,
		      GTK_SHADOW_OUT,
		      0, 0, -1, -1);
    }
}

static void
gtk_scrollpane_size_request(GtkWidget * widget, GtkRequisition * requisition)
{
  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(widget));
  g_return_if_fail(requisition != NULL);

/*  
 * requisition->width = 
 * (GTK_CONTAINER (widget)->border_width + 
 * GTK_WIDGET (widget)->style->klass->xthickness) * 2 +
 * widget->allocation.width;
 * requisition->height = 
 * (GTK_CONTAINER (widget)->border_width + 
 * GTK_WIDGET (widget)->style->klass->ythickness) * 2 +
 * widget->allocation.height;
 */
/*  
 * requisition->width = SCROLLPANE_DEFAULT_SIZE;
 * requisition->height = SCROLLPANE_DEFAULT_SIZE;
 */
}

static void
gtk_scrollpane_size_allocate(GtkWidget * widget, GtkAllocation * allocation)
{
  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(widget));
  g_return_if_fail(allocation != NULL);

  widget->allocation = *allocation;
  if (GTK_WIDGET_REALIZED(widget))
    gtk_scrollpane_update_slider_size(GTK_SCROLLPANE(widget));
}

static void
gtk_scrollpane_update_slider_size(GtkScrollpane * sp)
{
  gint                x, y, width, height;
  gint                x_offset, y_offset;
  GtkWidget          *widget;

  g_return_if_fail(sp != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(sp));

  widget = GTK_WIDGET(sp);
  if (!GTK_WIDGET_REALIZED(widget))
    return;
  
  width = widget->allocation.width;
  height = widget->allocation.height;
  x = widget->allocation.x;
  y = widget->allocation.y;

  sp->trough_width = width;
  sp->trough_height = height;
  /* we calculate the horizontal offset */
  /* we also do some sanity checking to guarentee that nothing weird has been passed in */
  /* (borrowed in spirit from gtk?scrollbar) */
  x_offset = XTHICK;
  sp->slider_width = width - XTHICK - XTHICK;
  if (sp->Xadjustment)
    {
      if ((sp->Xadjustment->page_size > 0) && (sp->Xadjustment->lower < sp->Xadjustment->upper))
	{
	  if (sp->Xadjustment->page_size < (sp->Xadjustment->upper - sp->Xadjustment->lower))
	    {
	      /* calculate the X offset... */
	      x_offset = ((gfloat) (width - XTHICK - XTHICK))
		* ((sp->Xadjustment->value - sp->Xadjustment->page_size / 2 - sp->Xadjustment->lower) /
		   (sp->Xadjustment->upper - sp->Xadjustment->lower)) + XTHICK;

	      /* scale the width appropriately */
	      sp->slider_width = ((gfloat) width) * sp->Xadjustment->page_size /
		(sp->Xadjustment->upper - sp->Xadjustment->lower);
	      if (sp->slider_width < sp->min_slider_width)
		sp->slider_width = sp->min_slider_width;
	      if (sp->slider_width + XTHICK + XTHICK > width)
		{
		  /* this takes care of rounding errors i think.  occasionally, there's a 1 pixel error */
		  x_offset = XTHICK;
		  sp->slider_width = width - XTHICK - XTHICK;
		}
	    }
	}
      /* FIXME: This is a hack to prevent an error somewhere in the above code ): */
      if (sp->slider_width + x_offset > width - XTHICK)
	sp->slider_width = width - XTHICK - x_offset;
    }

  y_offset = YTHICK;
  sp->slider_height = height - YTHICK - YTHICK;
  if (sp->Yadjustment)
    {
      if ((sp->Yadjustment->page_size > 0) && (sp->Yadjustment->lower < sp->Yadjustment->upper))
	{
	  if (sp->Yadjustment->page_size < (sp->Yadjustment->upper - sp->Yadjustment->lower))
	    {
	      /* calculate the Y offset... */
	      y_offset = ((gfloat) (height - YTHICK - YTHICK))
		* ((sp->Yadjustment->value - sp->Yadjustment->page_size / 2 - sp->Yadjustment->lower) /
		   (sp->Yadjustment->upper - sp->Yadjustment->lower)) + YTHICK;

	      /* scale the height appropriately */
	      sp->slider_height = ((gfloat) height) * sp->Yadjustment->page_size /
		(sp->Yadjustment->upper - sp->Yadjustment->lower);
	      if (height < sp->min_slider_height)
		sp->slider_height = sp->min_slider_height;
	      if (sp->slider_height + YTHICK + YTHICK > height)
		{
		  /* as per above... */
		  y_offset = YTHICK;
		  sp->slider_height = height - YTHICK - YTHICK;
		}
	    }
	}
      /* FIXME: This is a hack to prevent an error somewhere in the above code ): */
      if (sp->slider_height + y_offset > height - YTHICK)
	sp->slider_height = height - YTHICK - y_offset;
    }

  if (widget->window)
    gdk_window_move_resize(widget->window,
			   x, y, width, height);
  if (sp->slider)
    gdk_window_move_resize(sp->slider,
			   x_offset, y_offset, sp->slider_width, sp->slider_height);
}

static void
gtk_scrollpane_style_set(GtkWidget * widget, GtkStyle * previous_style)
{
  GtkScrollpane      *sp;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_SCROLLPANE(widget));

  sp = GTK_SCROLLPANE(widget);

  if (GTK_WIDGET_REALIZED(widget) && !GTK_WIDGET_NO_WINDOW(widget))
    if (sp->trough)
      {
	gtk_style_set_background(widget->style,
				 widget->window,
				 GTK_STATE_ACTIVE);
	if (GTK_WIDGET_DRAWABLE(widget))
	  gdk_window_clear(widget->window);
      }
}

/* Events... */
static              gint
gtk_scrollpane_expose(GtkWidget * widget, GdkEventExpose * event)
{
  GtkScrollpane      *sp;
  gint                xt;
  gint                yt;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_SCROLLPANE(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  sp = GTK_SCROLLPANE(widget);

  yt = YTHICK;
  xt = XTHICK;

  /* where did the expose occur??? */
  /* if it did in the trough, then... */
  if (event->window == sp->trough)
    {
      gint                xt = XTHICK;
      gint                yt = YTHICK;

      if (!((event->area.x >= xt) &&
	    (event->area.y >= yt) &&
	    (event->area.x + event->area.width <=
	     widget->allocation.width - xt) &&
	    (event->area.y + event->area.height <=
	     widget->allocation.height - xt)))
	gtk_scrollpane_draw_trough(sp);
    }
  else if (event->window == widget->window)
    {
      gtk_scrollpane_draw_background(sp);
    }
  else if (event->window == sp->slider)
    {
      gtk_scrollpane_draw_slider(sp);
    }
  return FALSE;
}

static              gint
gtk_scrollpane_button_press(GtkWidget * widget,
			    GdkEventButton * event)
{
  GtkScrollpane      *sp;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_SCROLLPANE(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  sp = GTK_SCROLLPANE(widget);
  if (!GTK_WIDGET_HAS_FOCUS(widget))
    gdk_pointer_grab(widget->window, FALSE,
		     GDK_BUTTON1_MOTION_MASK
		     | GDK_BUTTON_RELEASE_MASK,
		     NULL, NULL, event->time);
  if (event->button == 1)
    {
      if (event->window == sp->slider)
	{
	  sp->x_offset = event->x;
	  sp->y_offset = event->y;
	  sp->moving = TRUE;
	}
    }

  return TRUE;
}

static              gint
gtk_scrollpane_button_release(GtkWidget * widget,
			      GdkEventButton * event)
{
  GtkScrollpane      *sp;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_SCROLLPANE(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  sp = GTK_SCROLLPANE(widget);
  gdk_pointer_ungrab(event->time);

  if (event->button == 1)
    sp->moving = FALSE;
  else if (event->button == 2)
    gtk_signal_emit(GTK_OBJECT(sp), scrollpane_signals[MIDDLE_CLICKED]);
  else if (event->button == 3)
    gtk_signal_emit(GTK_OBJECT(sp), scrollpane_signals[RIGHT_CLICKED]);

  return TRUE;
}

GtkWidget          *
gtk_scrollpane_new(GtkAdjustment * Xadjustment,
		   GtkAdjustment * Yadjustment,
		   gint aspect_ratio)
{
  GtkScrollpane      *retval = gtk_type_new(gtk_scrollpane_get_type());

  retval->Xadjustment = Xadjustment;
  retval->Yadjustment = Yadjustment;
  retval->aspect_ratio = aspect_ratio;

  gtk_signal_connect(GTK_OBJECT(Xadjustment), "changed",
		     (GtkSignalFunc) gtk_scrollpane_adjustment_changed,
		     (gpointer) retval);
  gtk_signal_connect(GTK_OBJECT(Yadjustment), "changed",
		     (GtkSignalFunc) gtk_scrollpane_adjustment_changed,
		     (gpointer) retval);
  gtk_signal_connect(GTK_OBJECT(Xadjustment), "value_changed",
		     (GtkSignalFunc) gtk_scrollpane_value_adjustment_changed,
		     (gpointer) retval);
  gtk_signal_connect(GTK_OBJECT(Yadjustment), "value_changed",
		     (GtkSignalFunc) gtk_scrollpane_value_adjustment_changed,
		     (gpointer) retval);

  return GTK_WIDGET(retval);
}
gboolean 
gtk_scrollpane_step_up(GtkScrollpane * sp)
{
  g_return_val_if_fail(sp != NULL, FALSE);

  if (sp->Yadjustment->value - sp->Yadjustment->page_size / 2 - EPSILON <= sp->Yadjustment->lower)
    return FALSE;
  sp->Yadjustment->value -= 0.75 * (sp->Yadjustment->page_size);
  if (sp->Yadjustment->value - sp->Yadjustment->page_size / 2 - EPSILON <= sp->Yadjustment->lower)
    sp->Yadjustment->value = sp->Yadjustment->lower + sp->Yadjustment->page_size / 2;
  gtk_scrollpane_adjustment_value_changed(sp->Yadjustment);
  return TRUE;
}
gboolean 
gtk_scrollpane_step_down(GtkScrollpane * sp)
{
  g_return_val_if_fail(sp != NULL, FALSE);

  if (sp->Yadjustment->value + sp->Yadjustment->page_size / 2 + EPSILON >= sp->Yadjustment->upper)
    return FALSE;
  sp->Yadjustment->value += 0.75 * (sp->Yadjustment->page_size);
  if (sp->Yadjustment->value + sp->Yadjustment->page_size / 2 + EPSILON >= sp->Yadjustment->upper)
    sp->Yadjustment->value = sp->Yadjustment->upper - sp->Yadjustment->page_size / 2;
  gtk_scrollpane_adjustment_value_changed(sp->Yadjustment);
  return TRUE;
}
gboolean 
gtk_scrollpane_step_right(GtkScrollpane * sp)
{
  g_return_val_if_fail(sp != NULL, FALSE);

  if (sp->Xadjustment->value + sp->Xadjustment->page_size / 2 + EPSILON >= sp->Xadjustment->upper)
    return FALSE;
  sp->Xadjustment->value += 0.75 * (sp->Xadjustment->page_size);
  if (sp->Xadjustment->value + sp->Xadjustment->page_size / 2 + EPSILON >= sp->Xadjustment->upper)
    sp->Xadjustment->value = sp->Xadjustment->upper - sp->Xadjustment->page_size / 2;
  gtk_scrollpane_adjustment_value_changed(sp->Xadjustment);
  return TRUE;
}
gboolean 
gtk_scrollpane_step_left(GtkScrollpane * sp)
{
  g_return_val_if_fail(sp != NULL, FALSE);

  if (sp->Xadjustment->value - sp->Xadjustment->page_size / 2 - EPSILON <= sp->Xadjustment->lower)
    return FALSE;
  sp->Xadjustment->value -= 0.75 * (sp->Xadjustment->page_size);
  if (sp->Xadjustment->value - sp->Xadjustment->page_size / 2 - EPSILON <= sp->Xadjustment->lower)
    sp->Xadjustment->value = sp->Xadjustment->lower + sp->Xadjustment->page_size / 2;
  gtk_scrollpane_adjustment_value_changed(sp->Xadjustment);
  return TRUE;
}
static void
gtk_scrollpane_adjustment_changed(GtkAdjustment * adjustment, gpointer data)
{
  /*
   * we don't want to update if we caused the value change ourselves -- we just hope that
   * no one tries to update while we are updating ourselves...
   */
  if (GTK_SCROLLPANE(data)->moving)
    return;
  gtk_scrollpane_update_slider_size(GTK_SCROLLPANE(data));
}
static void
gtk_scrollpane_value_adjustment_changed(GtkAdjustment * adjustment, gpointer data)
{
  /*
   * we don't want to update if we caused the value change ourselves -- we just hope that
   * no one tries to update while we are updating ourselves...
   */
  if (GTK_SCROLLPANE(data)->moving)
    return;
  gtk_scrollpane_update_slider_size(GTK_SCROLLPANE(data));
}

static              gint
gtk_scrollpane_key_press(GtkWidget * widget, GdkEventKey * event)
{
  return TRUE;
}

static              gint
gtk_scrollpane_enter_notify(GtkWidget * widget, GdkEventCrossing * event)
{
  return TRUE;
}

static              gint
gtk_scrollpane_leave_notify(GtkWidget * widget, GdkEventCrossing * event)
{
  return TRUE;
}

static              gint
gtk_scrollpane_motion_notify(GtkWidget * widget, GdkEventMotion * event)
{
  GtkScrollpane      *sp;
  gint                x, y, width, height, depth;
  gfloat              value;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_SCROLLPANE(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  sp = GTK_SCROLLPANE(widget);

  if (sp->moving)
    {
      gdk_window_get_geometry(sp->slider, &x, &y, &width, &height, &depth);

      if (event->window == sp->trough)
	{
	  if (width + event->x - sp->x_offset > sp->trough_width - XTHICK)
	    x = sp->trough_width - XTHICK - width;
	  else if (event->x - sp->x_offset > XTHICK)
	    x = event->x - sp->x_offset;
	  else
	    x = XTHICK;

	  if (height + event->y - sp->y_offset > sp->trough_height - YTHICK)
	    y = sp->trough_height - YTHICK - height;
	  else if (event->y - sp->y_offset > YTHICK)
	    y = event->y - sp->y_offset;
	  else
	    y = YTHICK;
	  gdk_window_move(sp->slider, x, y);
	}
      else if (event->window == sp->slider)
	{

	  if (x + width + event->x - sp->x_offset > sp->trough_width - XTHICK)
	    x = sp->trough_width - XTHICK - width;
	  else if (x + event->x - sp->x_offset > XTHICK)
	    x += event->x - sp->x_offset;
	  else
	    x = XTHICK;

	  if (y + height + event->y - sp->y_offset > sp->trough_height - YTHICK)
	    y = sp->trough_height - YTHICK - height;
	  else if (y + event->y - sp->y_offset > YTHICK)
	    y += event->y - sp->y_offset;
	  else
	    y = XTHICK;

	  gdk_window_move(sp->slider, x, y);
	}

      /* first we check the x adjustment... */
      value = ((sp->Xadjustment->upper - sp->Xadjustment->lower) *
	       (x + ((gfloat) width) / 2) /
	       (sp->trough_width - XTHICK - XTHICK));
      if (value + sp->Xadjustment->page_size / 2 < sp->Xadjustment->upper)
	gtk_adjustment_set_value(sp->Xadjustment, value);
      else
	/*ugh -- yucky hack. to guarentee that value + page_size/2 < upper */
	gtk_adjustment_set_value(sp->Xadjustment, sp->Xadjustment->upper - sp->Xadjustment->page_size / 2 - 0.0001);

      /* then the y adjustment... */
      value = ((sp->Yadjustment->upper - sp->Yadjustment->lower) *
	       (y + ((gfloat) height) / 2) /
	       (sp->trough_height - YTHICK - YTHICK));
      if (value + sp->Yadjustment->page_size / 2 < sp->Yadjustment->upper)
	gtk_adjustment_set_value(sp->Yadjustment, value);
      else
	/*ugh -- yucky hack. to guarentee that value + page_size/2 < upper */
	gtk_adjustment_set_value(sp->Yadjustment, sp->Yadjustment->upper - sp->Yadjustment->page_size / 2 - 0.0001);
    }
  return TRUE;
}

static              gint
gtk_scrollpane_focus_in(GtkWidget * widget, GdkEventFocus * event)
{
  return TRUE;
}

static              gint
gtk_scrollpane_focus_out(GtkWidget * widget, GdkEventFocus * event)
{
  return TRUE;
}
