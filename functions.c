/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#include "functions.h"
#include "globals.h"
#include <ee_filesel.h>
#include <ee_file.h>
#include <ee_image.h>
#include <ee_list.h>
#include <ee_toolbar.h>
#include <ee_edit.h>
#include <ee_grab.h>
#include <ee_conf.h>
#include <gdk/gdkx.h>

static gchar loadsave = 0;
static gchar *oldfile = NULL;

static void
func_img_open(GtkWidget * widget, gpointer * data)
{
  GtkWidget *e;
  GdkImlibImage *im;
  gchar s[4096], *cmd, *f, **list;
  gint count, first = -1, i;

  if (!file_selector) 
    return;
  if (loadsave == 0)
    {
      if (isfile((gchar *)data))
	{
	  im = gdk_imlib_load_image((gchar *)data);
	  ee_image_set_filename(image_display, (gchar *)data);
	  ee_image_set_image(image_display, im);
	  ee_list_add_filename(image_list, (gchar *)data);
	}
      else if (isdir((gchar *)data))
	{
	  list = ls((gchar *)data, &count);
	  if (list)
	    {
	      for (i = 0; i < count; i++)
		{
		  g_snprintf(s, sizeof(s), "%s/%s", 
			     (gchar *)data, list[i]);
		  if (isfile(s))
		    {
		      if (first < 0)
			first = i;
		      ee_list_add_filename(image_list, s);
		    }
		}
	      if (first >= 0)
		{
		  g_snprintf(s, sizeof(s), "%s/%s", 
			     (gchar *)data, list[first]);
		  im = gdk_imlib_load_image(s);
		  ee_image_set_filename(image_display, s);
		  ee_image_set_image(image_display, im);
		}
	      freestrlist(list, count);
	    }
	}
      gtk_widget_hide(file_selector);
    }
  else if (loadsave == 2)
    {
      gtk_widget_hide(file_selector);
      ee_filesel_unset_printout(file_selector);
      f = ee_filesel_get_filename(file_selector);
      e = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "actual_entry");
      cmd = gtk_entry_get_text(GTK_ENTRY(e));
      g_snprintf(s, sizeof(s), "cat %s | %s", f, cmd);
      system(s);
      rm(f);
      ee_filesel_set_filename(file_selector, oldfile);
    }
  widget = NULL;
  data = NULL;
}

void
func_crop_image(GtkWidget *widget, gpointer data)
{
  ee_image_do_crop(image_display);
  widget = NULL;
  data = NULL;
}

void
func_open_image(GtkWidget *widget, gpointer data)
{
  loadsave = 0;
  if (!file_selector)
    {
      file_selector = ee_filesel_new();
      gtk_signal_connect(GTK_OBJECT(file_selector), "ok_clicked",
			 GTK_SIGNAL_FUNC(func_img_open), NULL);
    }
  ee_filesel_unset_printout(file_selector);
  ee_filesel_hide_savedialog(file_selector);
  gtk_widget_show(file_selector);
  widget = NULL;
  data = NULL;
}

void
func_save_image(GtkWidget *widget, gpointer data)
{ 
  GdkImlibImage *im;
  
  if (!image_display) 
    return;
  im = (GdkImlibImage *)ee_image_get_image(image_display);
  if (!im)
    return;
  loadsave = 1;
  if (!file_selector)
    {
      file_selector = ee_filesel_new();
      gtk_signal_connect(GTK_OBJECT(file_selector), "ok_clicked",
			 GTK_SIGNAL_FUNC(func_img_open), NULL);
    }
  ee_filesel_unset_printout(file_selector);
  ee_filesel_show_savedialog(file_selector);
  gtk_widget_show(file_selector);
  ee_filesel_set_save_image(file_selector, im);
  widget = NULL;
  data = NULL;
}

void
func_print_image(GtkWidget *widget, gpointer data)
{ 
  GdkImlibImage *im;
  gchar s[1024];
  
  if (!image_display) 
    return;
  im = (GdkImlibImage *)ee_image_get_image(image_display);
  if (!im)
    return;
  loadsave = 2;
  if (!file_selector)
    {
      file_selector = ee_filesel_new();
      gtk_signal_connect(GTK_OBJECT(file_selector), "ok_clicked",
			 GTK_SIGNAL_FUNC(func_img_open), NULL);
    }
  ee_filesel_show_savedialog(file_selector);
  ee_filesel_set_save_image(file_selector, im);
  ee_filesel_set_printout(file_selector);
  g_snprintf(s, sizeof(s), "/tmp/%i_%i.ps",getuid(), (int)time(NULL));
  oldfile = ee_filesel_get_filename(file_selector);
  ee_filesel_set_filename(file_selector, s);
  gtk_widget_show(file_selector);
  widget = NULL;
  data = NULL;
}

void
func_save_image_only(GtkWidget *widget, gpointer data)
{
  GdkImlibImage *im;
  gchar *file;
  
  if (!image_display) 
    return;
  im = (GdkImlibImage *)ee_image_get_image(image_display);
  if (im)
    {
      file = ee_image_get_filename(image_display);
      if (file)
	{
	  gdk_imlib_save_image(im, file, NULL);
	}
    }
  widget = NULL;
  data = NULL;
}

void
func_hide_toolbar(GtkWidget *widget, gpointer data)
{
  if (image_toolbar)
    gtk_widget_destroy(image_toolbar);
  if (padder_button)
    gtk_widget_destroy(padder_button);
  gtk_object_set_data(GTK_OBJECT(image_display), "toolbar_pos", GINT_TO_POINTER (0));
  padder_button = NULL;
  image_toolbar = NULL;
  widget = NULL;
  data = NULL;
}

void
func_add_toolbar_top(GtkWidget *widget, gpointer data)
{
  func_hide_toolbar(NULL, NULL);
  image_toolbar = ee_toolbar_new();
  ee_image_add_toolbar(image_display, image_toolbar, 0);
  widget = NULL;
  data = NULL;
}

void
func_add_toolbar_bottom(GtkWidget *widget, gpointer data)
{
  func_hide_toolbar(NULL, NULL);
  image_toolbar = ee_toolbar_new();
  ee_image_add_toolbar(image_display, image_toolbar, 1);
  widget = NULL;
  data = NULL;
}

void
func_add_toolbar_left(GtkWidget *widget, gpointer data)
{
  func_hide_toolbar(NULL, NULL);
  image_toolbar = ee_toolbar_new();
  ee_image_add_toolbar(image_display, image_toolbar, 2);
  widget = NULL;
  data = NULL;
}

void
func_add_toolbar_right(GtkWidget *widget, gpointer data)
{
  func_hide_toolbar(NULL, NULL);
  image_toolbar = ee_toolbar_new();
  ee_image_add_toolbar(image_display, image_toolbar, 3);
  widget = NULL;
  data = NULL;
}

void
func_toggle_scrollable(GtkWidget *widget, gpointer data)
{
  gint                 forcescroll;

  forcescroll = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(image_display), "forcescroll"));
  if (forcescroll)
    ee_image_force_scrollable(image_display, 0);
  else
    ee_image_force_scrollable(image_display, 1);
  widget = NULL;
  data = NULL;
}

void
func_exit(GtkWidget *widget, gpointer data)
{
  ee_conf_save();
  exit(0);
  widget = NULL;
  data = NULL;
}

void
func_first(GtkWidget *widget, gpointer data)
{
  if (image_list)
    ee_list_start(NULL, image_list);
  widget = NULL;
  data = NULL;
}

void
func_prev(GtkWidget *widget, gpointer data)
{
  if (image_list)
    ee_list_prev(NULL, image_list);
  widget = NULL;
  data = NULL;
}

void
func_next(GtkWidget *widget, gpointer data)
{
  if (image_list)
    ee_list_next(NULL, image_list);
  widget = NULL;
  data = NULL;
}

void
func_last(GtkWidget *widget, gpointer data)
{
  if (image_list)
    ee_list_end(NULL, image_list);
  widget = NULL;
  data = NULL;
}

void
func_config(GtkWidget *widget, gpointer data)
{
  ee_conf_save();
  widget = NULL;
  data = NULL;
}

void
func_image_halve(GtkWidget *widget, gpointer data)
{
  gint w, h;
  GdkImlibImage *im;
  gint scrolled;
  
  im = ee_image_get_image(image_display);
  scrolled = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(image_display), "scrollable"));
  if (im)
    {
      ee_image_get_size(image_display, &w, &h);
      ee_image_set_size(image_display, w / 2, h / 2);
      if (scrolled)
	ee_image_set_image_size(image_display, w / 2, h / 2);
    }
  widget = NULL;
  data = NULL;
}

void
func_image_m10(GtkWidget *widget, gpointer data)
{
  gint w, h;
  GdkImlibImage *im;
  gint scrolled;
  
  im = ee_image_get_image(image_display);
  scrolled = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(image_display), "scrollable"));
  if (im)
    {
      ee_image_get_size(image_display, &w, &h);
      ee_image_set_size(image_display, (w * 9) / 10, (h * 9) / 10);
      if (scrolled)
	ee_image_set_image_size(image_display, (w * 9) / 10, (h * 9) / 10);
    }
  widget = NULL;
  data = NULL;
}

void
func_image_normal(GtkWidget *widget, gpointer data)
{
  gint w, h;
  GdkImlibImage *im;
  gint scrolled;
  
  im = ee_image_get_image(image_display);
  scrolled = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(image_display), "scrollable"));
  if (im)
    {
      w = im->rgb_width;
      h = im->rgb_height;
      ee_image_set_size(image_display, w, h);
      if (scrolled)
	ee_image_set_image_size(image_display, w, h);
    }
  widget = NULL;
  data = NULL;
}

void
func_image_p10(GtkWidget *widget, gpointer data)
{
  gint w, h;
  GdkImlibImage *im;
  gint scrolled;
  
  im = ee_image_get_image(image_display);
  scrolled = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(image_display), "scrollable"));
  if (im)
    {
      ee_image_get_size(image_display, &w, &h);
      ee_image_set_size(image_display, (w * 11) / 10, (h * 11) / 10);
      if (scrolled)
	ee_image_set_image_size(image_display, (w * 11) / 10, (h * 11) / 10);
    }
  widget = NULL;
  data = NULL;
}

void
func_image_double(GtkWidget *widget, gpointer data)
{
  gint w, h;
  GdkImlibImage *im;
  gint scrolled;
  
  im = ee_image_get_image(image_display);
  scrolled = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(image_display), "scrollable"));
  if (im)
    {
      ee_image_get_size(image_display, &w, &h);
      ee_image_set_size(image_display, w * 2, h * 2);
      if (scrolled)
	ee_image_set_image_size(image_display, w * 2, h * 2);
    }
  widget = NULL;
  data = NULL;
}

void
func_image_grab(GtkWidget *widget, gpointer data)
{
  gint hide = 0;
  GdkImlibImage *im;
  gint w, h;
  gchar show_list = 0, show_edit = 0;
  
  if (hide)
    {
      if ((image_display) && (GTK_WIDGET_VISIBLE(image_display)))
	gtk_widget_hide(image_display);
      if ((file_selector) && (GTK_WIDGET_VISIBLE(file_selector)))
	gtk_widget_hide(file_selector);
      if ((image_list) && (GTK_WIDGET_VISIBLE(image_list)))
	{
	  show_list = 1;
	  gtk_widget_hide(image_list);
	}
      if ((edit_window) && (GTK_WIDGET_VISIBLE(edit_window)))
	{
	  show_edit = 1;
	  gtk_widget_hide(edit_window);
	}
      if ((main_menu) && (GTK_WIDGET_VISIBLE(main_menu)))
	gtk_widget_hide(main_menu);
    }
  while (gtk_events_pending())
    gtk_main_iteration();
  
  w = gdk_screen_width();  
  h = gdk_screen_height();  
  im = gdk_imlib_create_image_from_drawable((GdkWindow *)&gdk_root_parent, 
					    NULL, 0, 0, w, h);

  ee_image_set_filename(image_display, "DisplayGrab.ppm");
  ee_image_set_image(image_display, im);
  ee_image_set_filename(image_display, "DisplayGrab.ppm");  
  
  if (hide)
    {
      gtk_widget_show(image_display);
      if (show_list)
	gtk_widget_show(image_list);
      if (show_edit)
	gtk_widget_show(edit_window);
    }
  widget = NULL;
  data = NULL;
}

void
func_show_edit(GtkWidget *widget, gpointer data)
{
  if (!edit_window)
    edit_window = ee_edit_new();
  if (GTK_WIDGET_VISIBLE(edit_window))
    gtk_widget_hide(edit_window);
  else
    gtk_widget_show(edit_window);
  ee_conf_set_options();
  widget = NULL;
  data = NULL;
}

void
func_show_list(GtkWidget *widget, gpointer data)
{
  if (GTK_WIDGET_VISIBLE(image_list))
    gtk_widget_hide(image_list);
  else
    gtk_widget_show(image_list);
  widget = NULL;
  data = NULL;
}

void
func_flip_h(GtkWidget *widget, gpointer data)
{
  GdkImlibImage *im;
  gint w, h;
  
  im = ee_image_get_image(image_display);
  if (im)
    {
      gdk_imlib_flip_image_horizontal(im);
      ee_image_get_size(image_display, &w, &h);
      ee_image_set_size(image_display, w, h);
      ee_image_set_image_size(image_display, w, h);
    }
  widget = NULL;
  data = NULL;
}

void
func_flip_v(GtkWidget *widget, gpointer data)
{
  GdkImlibImage *im;
  gint w, h;
  
  im = ee_image_get_image(image_display);
  if (im)
    {
      gdk_imlib_flip_image_vertical(im);
      ee_image_get_size(image_display, &w, &h);
      ee_image_set_size(image_display, w, h);
      ee_image_set_image_size(image_display, w, h);
    }
  widget = NULL;
  data = NULL;
}

void
func_flip_z(GtkWidget *widget, gpointer data)
{
  GdkImlibImage *im;
  gint w, h;
  
  im = ee_image_get_image(image_display);
  if (im)
    {
      gdk_imlib_rotate_image(im, 1);
      gdk_imlib_flip_image_vertical(im);
      ee_image_get_size(image_display, &w, &h);
      ee_image_set_size(image_display, h, w);
      ee_image_set_image_size(image_display, h, w);
    }
  widget = NULL;
  data = NULL;
}

void
func_appliy_size(GtkWidget *widget, gpointer data)
{
  GdkImlibImage *im, *im2;
  gint w, h;
  
  im = ee_image_get_image(image_display);
  if (im)
    {
      ee_image_get_size(image_display, &w, &h);
      im2 = gdk_imlib_clone_scaled_image(im, w, h);
      ee_image_set_image(image_display, im2);
    }
  widget = NULL;
  data = NULL;
}

void
func_image_max_size(GtkWidget *widget, gpointer data)
{
  GdkImlibImage *im;
  gint scrolled;
  
  im = ee_image_get_image(image_display);
  scrolled = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(image_display), "scrollable"));
  if (im)
    {
      ee_image_set_size(image_display, 
			gdk_screen_width(), gdk_screen_height());
      if (scrolled)
	ee_image_set_image_size(image_display,
				gdk_screen_width(), gdk_screen_height());
    }
  widget = NULL;
  data = NULL;
}

void
func_image_max_aspect(GtkWidget *widget, gpointer data)
{
  gint w, h, sw, sh, nw, nh;
  GdkImlibImage *im;
  gint scrolled;
  
  im = ee_image_get_image(image_display);
  scrolled = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(image_display), "scrollable"));
  if (im)
    {
      w = im->rgb_width;
      h = im->rgb_height;
      sw = gdk_screen_width();
      sh = gdk_screen_height();
      nw = 0; 
      nh = 0;
      if (((w << 16) / h) > ((sw << 16) / sh))
	{
	  nw = sw;
	  nh = (h * sw) / w;
	}
      else
	{
	  nw = (w * sh) / h;
	  nh = sh;
	}
      ee_image_set_size(image_display, nw, nh);
      if (scrolled)
	ee_image_set_image_size(image_display, nw, nh);
    }
  widget = NULL;
  data = NULL;
}

void
func_image_set_size(GtkWidget *widget, gpointer data)
{
  gint w, h, sw, sh, nw, nh;
  GdkImlibImage *im;
  gint scrolled;
  
  im = ee_image_get_image(image_display);
  scrolled = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(image_display), "scrollable"));
  if (im)
    {
      w = im->rgb_width;
      h = im->rgb_height;
      sw = gdk_screen_width();
      sh = gdk_screen_height();
      nw = 0; 
      nh = 0;
      if (((w << 16) / h) > ((sw << 16) / sh))
	{
	  nw = sw;
	  nh = (h * sw) / w;
	}
      else
	{
	  nw = (w * sh) / h;
	  nh = sh;
	}
      ee_image_set_size(image_display, nw, nh);
      if (scrolled)
	ee_image_set_image_size(image_display, nw, nh);
    }
  widget = NULL;
  data = NULL;
}

void
func_window_grab(GtkWidget *widget, gpointer data)
{
  GtkWidget *w;
  GtkWidget *d, *l;

  d = gnome_dialog_new(_("Select a window"),
		       GNOME_STOCK_BUTTON_OK,
		       NULL);
  l = gtk_label_new(_("Please press OK then click on a window to grab"));
  gtk_widget_show(l);
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), l, TRUE, TRUE, 5);
  gtk_window_set_modal(GTK_WINDOW(d),TRUE);
  gtk_window_position(GTK_WINDOW(d), GTK_WIN_POS_MOUSE);
  gnome_dialog_run(GNOME_DIALOG(d));
  gtk_widget_destroy(d);
  while (gtk_events_pending())
    gtk_main_iteration();
  gdk_flush();
  ee_grab_select_window();
  w = ee_grab_new();
  gtk_widget_show(w);
  widget = NULL;
  data = NULL;
}

void
func_set_root(GtkWidget *widget, gpointer data)
{
  GdkVisual          *gvis;
  GdkWindow          *rwin;
  GdkPixmap          *pmap;  
  gint w, h;
  GdkImlibImage *im;
  
  rwin = (GdkWindow *) & gdk_root_parent;
  gvis = gdk_window_get_visual(rwin);
  if (gvis != gdk_imlib_get_visual())
    return;    
  
  im = ee_image_get_image(image_display);
  if (im)
    {
      ee_image_get_size(image_display, &w, &h);
      gdk_imlib_render(im, w, h);
      pmap = gdk_imlib_move_image(im);
      gdk_window_set_back_pixmap(rwin, pmap, FALSE);
      gdk_window_clear(rwin);
      gdk_imlib_free_pixmap(pmap);
    }
  gdk_flush();
  widget = NULL;
  data = NULL;
}
