/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#include <ee_menu.h>
#include <stdio.h>
#include <stdlib.h>
#include "functions.h"
#include "globals.h"

static GnomeUIInfo  menu_file[] =
{
  GNOMEUIINFO_ITEM_STOCK(N_("Open"), 
			 N_("Open up a new image file"), 
			 func_open_image,
			 GNOME_STOCK_MENU_OPEN),
  GNOMEUIINFO_ITEM_STOCK(N_("Save"), 
			 N_("Save this image"), 
			 func_save_image_only, 
			 GNOME_STOCK_MENU_SAVE),
  GNOMEUIINFO_ITEM_STOCK(N_("Save As"), 
			 N_("Save this image as a new file"), 
			 func_save_image, 
			 GNOME_STOCK_MENU_SAVE_AS),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(N_("Print"), 
			 N_("Print this Image"), 
			 func_print_image, 
			 GNOME_STOCK_MENU_PRINT),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(N_("Last Image"), 
			 N_("Display the last image in the image list"), 
			 func_last,
			 GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_ITEM_STOCK(N_("Next Image"), 
			 N_("Display the next image in the image list"), 
			 func_next,
			 GNOME_STOCK_MENU_FORWARD),
  GNOMEUIINFO_ITEM_STOCK(N_("Previous Image"), 
			 N_("Display the previous image in the image list"), 
			 func_prev,
			 GNOME_STOCK_MENU_BACK),
  GNOMEUIINFO_ITEM_STOCK(N_("First Image"), 
			 N_("Display the first image in the image list"), 
			 func_first,
			 GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(N_("Set as Desktop Background"), 
			 N_("Set as Desktop Background"), 
			 func_set_root,
			 GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(N_("Quit"), 
			 N_("Exit this program"), 
			 func_exit, 
			 GNOME_STOCK_MENU_QUIT),
  GNOMEUIINFO_END
};

static GnomeUIInfo  menu_toolbar[] =
{
  GNOMEUIINFO_ITEM_STOCK(N_("Top"), 
			 N_("Add Toolbar To Top"), 
			 func_add_toolbar_top,
			 GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_ITEM_STOCK(N_("Bottom"), 
			 N_("Add Toolbar To Bottom"), 
			 func_add_toolbar_bottom,
			 GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_ITEM_STOCK(N_("Left"), 
			 N_("Add Toolbar To Left"), 
			 func_add_toolbar_left,
			 GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_ITEM_STOCK(N_("Right"), 
			 N_("Add Toolbar To Right"), 
			 func_add_toolbar_right,
			 GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(N_("Hide"), 
			 N_("Hide the Toolbar"), 
			 func_hide_toolbar,
			 GNOME_STOCK_MENU_CLOSE),
  GNOMEUIINFO_END
};

static GnomeUIInfo  menu_view[] =
{
  GNOMEUIINFO_ITEM_STOCK(N_("Show/Hide Edit Window"), 
			 N_("Toggle Edit Window visability"), 
			 func_show_edit,
			 GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_ITEM_STOCK(N_("Show/Hide List Window"), 
			 N_("Toggle List Window visability"), 
			 func_show_list,
			 GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_ITEM_STOCK(N_("Toggle Scrolled View"), 
			 N_("Toggle Scrollbars in a scrolled view"), 
			 func_toggle_scrollable,
			 GNOME_STOCK_MENU_BLANK),
#if 0
  GNOMEUIINFO_SUBTREE(N_("Tool Bar"), 
		      menu_toolbar),
#endif
  GNOMEUIINFO_END
};

static GnomeUIInfo  menu_main[] =
{
    GNOMEUIINFO_SUBTREE(N_("File"), 
			menu_file),
    GNOMEUIINFO_SUBTREE(N_("View"), 
			menu_view),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_ITEM_STOCK(N_("Crop Selection"), 
			   N_("Crop the current crop selection"), 
			   func_crop_image,
			   GNOME_STOCK_MENU_CUT),
    GNOMEUIINFO_ITEM_STOCK(N_("Save Settings"), 
			   N_("Save the current settings"), 
			   func_config,
			   GNOME_STOCK_MENU_PREF),
/*  
    GNOMEUIINFO_SUBTREE(N_("Help"), 
			menu_empty),*/
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_ITEM_STOCK(N_("Quit"), 
			   N_("Exit this program"), 
			   func_exit, 
			   GNOME_STOCK_MENU_QUIT),
    GNOMEUIINFO_END
};

static GnomeUIInfo  menu_dummy[] =
{
    GNOMEUIINFO_SUBTREE("Main", 
			menu_main),
    GNOMEUIINFO_END
};


GtkWidget          *
ee_menu_new(void)
{
  GtkWidget *w;
  GtkMenuItem *mi;
  
  ee_app = gnome_app_new("ee", "ElectricEyes");
  gnome_app_create_menus(GNOME_APP(ee_app), menu_dummy);
  mi = GTK_MENU_ITEM(menu_dummy[0].widget);
  w = GTK_WIDGET(mi->submenu);
  return w;
}

