/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#ifndef __EE_FILE_H__
#define __EE_FILE_H__

#ifdef __cplusplus
extern              "C"
{
#endif                          /* __cplusplus */
void                md(char *s);
int                 exists(char *s);
int                 isfile(char *s);
int                 isdir(char *s);
char              **ls(char *dir, int *num);
void                freestrlist(char **l, int num);
void                rm(char *s);
void                mv(char *s, char *ss);
void                cp(char *s, char *ss);
unsigned long       moddate(char *s);
int                 filesize(char *s);
void                cd(char *s);
char               *cwd(void);
int                 permissions(char *s);
int                 owner(char *s);
int                 group(char *s);
char               *username(int uid);
char               *homedir(int uid);
char               *usershell(int uid);
char               *atword(char *s, int num);
char               *atchar(char *s, char c);
void                word(char *s, int num, char *wd);
int                 canread(char *s);
int                 canwrite(char *s);
int                 canexec(char *s);
char               *fileof(char *s);
char               *fullfileof(char *s);
char               *noext(char *s);
char               *extension(char *file);
void                mkdirs(char *s);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif
