/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#ifndef __EE_CONF_H__
#define __EE_CONF_H__

#include <gtk/gtk.h>
#include <gdk_imlib.h>
#include <gnome.h>

#ifdef __cplusplus
extern              "C"
{
#endif                          /* __cplusplus */
void           ee_conf_save(void);
void           ee_conf_load(void);
void           ee_conf_set_options(void);
#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif
