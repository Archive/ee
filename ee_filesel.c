/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#include <config.h>
#include <ee_filesel.h>
#include <ee_file.h>
#include <stdio.h>
#include <stdlib.h>
#include <gnome.h>
#include <gtkscrollpane.h>

static void
ee_filesel_cb_ok(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_loadall(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_kill(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_page_adj_change(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_scale_change(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_quality_change(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_set_page(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_color_toggle(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_set_format(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_toggle(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_select(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_destroy(GtkWidget * widget, gpointer * data);
static void
ee_filesel_cb_cancel(GtkWidget * widget, gpointer * data);

/* Signal id for ok_clicked signal */

static guint ok_clicked_id = 0;

static void
ee_adjustment_changed(GtkAdjustment *a)
{
/*  gtk_adjustment_changed(a);*/
  gtk_signal_emit_by_name(GTK_OBJECT(a), "changed");
}

void
ee_filesel_show_savedialog(GtkWidget *widget)
{
  GtkWidget          *w;

  gtk_object_set_data(GTK_OBJECT(widget), "dosave", GINT_TO_POINTER (1));
  w = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(widget), "save"));
  gtk_widget_show(w);
  w = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(widget), "load_all"));
  gtk_widget_hide(w);
  gtk_window_set_title(GTK_WINDOW(widget), _("Select a file to save"));
}

void
ee_filesel_hide_savedialog(GtkWidget *widget)
{
  GtkWidget          *w;
  
  gtk_object_set_data(GTK_OBJECT(widget), "dosave", GINT_TO_POINTER (0));
  w = gtk_object_get_data(GTK_OBJECT(widget), "save");
  gtk_widget_hide(w);
  w = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(widget), "load_all"));
  gtk_widget_show(w);
  gtk_window_set_title(GTK_WINDOW(widget), _("Select a file to load"));
}

void
ee_filesel_set_printout(GtkWidget *widget)
{
  GtkWidget *f;
  gint preview;
  
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "filesel");
  if (!f)
    return;
  gtk_widget_hide(GTK_FILE_SELECTION(f)->main_vbox);
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "outerframe");
  preview = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(widget), "preview"));
  if (preview)
    gtk_widget_hide(f);
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "optionmenu");
  gtk_widget_hide(f);
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "print_button");
  gtk_widget_show(f);
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "actual_entry");
  gtk_entry_set_text(GTK_ENTRY (f), "lpr");
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "entry");
  gtk_widget_show(f);
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "image_format");
  gtk_widget_show(f);
  gtk_window_set_title(GTK_WINDOW(widget), _("Set Printout settings"));
}

void
ee_filesel_unset_printout(GtkWidget *widget)
{
  GtkWidget *f;
  gint preview;
  
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "filesel");
  if (!f)
    return;
  gtk_widget_show(GTK_FILE_SELECTION(f)->main_vbox);
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "outerframe");
  preview = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(widget), "preview"));
  if (preview)
    gtk_widget_show(f);
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "optionmenu");
  gtk_widget_show(f);
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "print_button");
  gtk_widget_hide(f);
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "entry");
  gtk_widget_hide(f);
  f = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "image_format");
  gtk_widget_hide(f);
  gtk_window_set_title(GTK_WINDOW(widget), _("Select a file to load"));
}

gchar *
ee_filesel_get_filename(GtkWidget *widget)
{
  GtkWidget          *f;
  
  f = gtk_object_get_data(GTK_OBJECT(widget), "filesel");
  return gtk_file_selection_get_filename(GTK_FILE_SELECTION(f));
}

void
ee_filesel_set_filename(GtkWidget *widget, gchar *file)
{
  GtkWidget          *f;
  
  f = gtk_object_get_data(GTK_OBJECT(widget), "filesel");
  gtk_file_selection_set_filename(GTK_FILE_SELECTION(f), file);
}

void
ee_filesel_set_save_image(GtkWidget *widget, GdkImlibImage *im)
{
  GtkAdjustment      *adj;
  
  adj = gtk_object_get_data(GTK_OBJECT(widget), "scale_adj");
  gtk_object_set_data(GTK_OBJECT(widget), "save_image", im);
  adj->value = 100;
  ee_adjustment_changed(adj);
  ee_filesel_set_filename(widget, im->filename);
  ee_filesel_cb_scale_change(widget, (gpointer)widget);
  ee_filesel_show_savedialog(widget);
}

static void
ee_filesel_cb_ok(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w;
  gint                save;
  gchar              *curfile;
  GdkImlibImage      *im;
  GdkImlibSaveInfo   *si;
  
  w = GTK_WIDGET(data);
  si = (GdkImlibSaveInfo *)gtk_object_get_data(GTK_OBJECT(w), "save_info");
  save = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(w), "dosave"));
  im = (GdkImlibImage *)gtk_object_get_data(GTK_OBJECT(w), "save_image");
  curfile = ee_filesel_get_filename(w);
  if (save)
    {
      gdk_imlib_save_image(im, curfile, si);
      gtk_signal_emit (GTK_OBJECT(w), ok_clicked_id, NULL);
    }
  else
    gtk_signal_emit (GTK_OBJECT(w), ok_clicked_id, curfile);
  gtk_widget_hide(w);
  widget = NULL;
}

static void
ee_filesel_cb_loadall(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w;
  gchar              *curfile;
  int i;
  
  w = GTK_WIDGET(data);
  curfile = ee_filesel_get_filename(w);

  i = strlen(curfile) -1;
  while (i >= 0)
    {
      if (curfile[i] == '/')
	{
	  curfile[i] = 0;
	  i = 0;
	}
      else
	curfile[i] = 0;
      i--;
    }
  gtk_signal_emit (GTK_OBJECT(w), ok_clicked_id, curfile);
  widget = NULL;
}

static void
ee_filesel_cb_kill(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w;
  GdkImlibSaveInfo   *si;
  
  w = GTK_WIDGET(data);
  si = gtk_object_get_data(GTK_OBJECT(w), "save_info");
  g_free(si);
  widget = NULL;
}

static void
ee_filesel_cb_page_adj_change(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w;
  GtkAdjustment      *adj;
  GtkAdjustment      *adj2;
  GdkImlibSaveInfo   *si;

  w = GTK_WIDGET(data);
  adj = gtk_object_get_data(GTK_OBJECT(w), "page_adj");
  adj2 = gtk_object_get_data(GTK_OBJECT(w), "page_adj2");
  si = gtk_object_get_data(GTK_OBJECT(w), "save_info");
  si->xjustification = (int)((1024 * adj->value) / 1);
  si->yjustification = 1024 - (int)((1024 * adj2->value) / 1);
  widget = NULL;
}

static void
ee_filesel_cb_scale_change(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w;
  GtkAdjustment      *adj;
  GtkAdjustment      *adj2;
  GtkAdjustment      *adj3;
  GdkImlibSaveInfo   *si;
  GdkImlibImage      *im;
  gint                ww, hh;
  
  w = GTK_WIDGET(data);
  adj = gtk_object_get_data(GTK_OBJECT(w), "scale_adj");
  adj2 = gtk_object_get_data(GTK_OBJECT(w), "page_adj");
  adj3 = gtk_object_get_data(GTK_OBJECT(w), "page_adj2");
  im = gtk_object_get_data(GTK_OBJECT(w), "save_image");
  si = gtk_object_get_data(GTK_OBJECT(w), "save_info");
  si->scaling = (1024 * (int)adj->value) / 100;
  if (im)
    {
      if ((im->rgb_height * 3) < (im->rgb_width * 4))
	{
	  ww = 64;
	  hh = (3 * 64 * im->rgb_height) / (4 * im->rgb_width);
	}
      else
	{
	  hh = 64;
	  ww = (4 * 64 * im->rgb_width) / (3 * im->rgb_height);
	}
      adj2->page_size = (gfloat)((si->scaling * ww) / 64) / 1024;
      adj3->page_size = (gfloat)((si->scaling * hh) / 64) / 1024;
      ee_adjustment_changed(adj2);
      ee_adjustment_changed(adj3);
    }
  widget = NULL;
}

static void
ee_filesel_cb_quality_change(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w;
  GtkAdjustment      *adj;
  GdkImlibSaveInfo   *si;

  w = GTK_WIDGET(data);
  adj = gtk_object_get_data(GTK_OBJECT(w), "quality_adj");
  si = gtk_object_get_data(GTK_OBJECT(w), "save_info");
  si->quality = (int)((255 * adj->value) / 100);
  widget = NULL;
}

static void
ee_filesel_cb_set_page(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w;
  GdkImlibSaveInfo   *si;

  w = GTK_WIDGET(data);
  si = (GdkImlibSaveInfo *)gtk_object_get_data(GTK_OBJECT(w), "save_info");
  si->page_size = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(widget), "data"));
  widget = NULL;
}

static void
ee_filesel_cb_color_toggle(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w;
  GdkImlibSaveInfo   *si;

  w = GTK_WIDGET(data);
  si = (GdkImlibSaveInfo *)gtk_object_get_data(GTK_OBJECT(w), "save_info");
  if (si->color)
    si->color = 0;
  else
    si->color = 1;
  widget = NULL;
}

static void
ee_filesel_cb_set_format(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w, *f;
  gchar              *file, *file2, tmp[1024], *ext;
  gint                save;

  w = GTK_WIDGET(data);
  f = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(w), "filesel"));
  ext = (gchar *)gtk_object_get_data(GTK_OBJECT(widget), "extension");
  save = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(widget), "dosave"));

  if (save)
    return;

  file = gtk_file_selection_get_filename(GTK_FILE_SELECTION(f));
  if (!g_strcasecmp("???", ext))
    return;
  if (!g_strcasecmp(extension(file), ext))
    return;
  file2 = noext(file);
  g_snprintf(tmp, sizeof(tmp), "%s.%s", file2, ext);
  gtk_file_selection_set_filename(GTK_FILE_SELECTION(f), tmp);
  g_free(file2);
}

static void
ee_filesel_cb_toggle(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w, *f;
  gint                preview;

  w = GTK_WIDGET(data);
  f = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(w), "outerframe"));
  preview = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(w), "preview"));

  if (preview)
    {
      gtk_widget_hide(f);
      gtk_object_set_data(GTK_OBJECT(w), "preview", GINT_TO_POINTER (0));
    }
  else
    {
      gtk_widget_show(f);
      gtk_object_set_data(GTK_OBJECT(w), "preview", GINT_TO_POINTER (1));
    }
  widget = NULL;
}

static void
ee_filesel_cb_select(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w, *f, *a, *c, *b, *om, *q, *p;
  gchar              *ext, *file, *file2, tmp[1024], *curfile;
  GdkImlibImage      *im = NULL;
  gint                ww, hh, preview, i;
  GdkPixmap          *pmap = NULL, *mask = NULL;
  time_t              t;

  w = GTK_WIDGET(data);
  a = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(w), "mini"));
  b = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(w), "frame"));
  c = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(w), "info"));
  om = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(w), "optionmenu"));
  f = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(w), "filesel"));
  q = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(w), "quality"));
  p = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(w), "page"));
  preview = GPOINTER_TO_INT (gtk_object_get_data(GTK_OBJECT(w), "preview"));
  curfile = (gchar *)gtk_object_get_data(GTK_OBJECT(w), "filename");
  file = (gchar *)gtk_file_selection_get_filename(GTK_FILE_SELECTION(f));

  if (file)
    {

      if ((curfile) && (!strcmp(file, curfile)))
	return;
      if (curfile)
	g_free(curfile);

      gtk_object_set_data(GTK_OBJECT(w), "filename", (gpointer)g_strdup(file));

      ext = extension(file);
      if ((!g_strcasecmp(ext, "ppm")) ||
	  (!g_strcasecmp(ext, "pnm")))
	{
	  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 0);
	  gtk_widget_hide(p);
	  gtk_widget_hide(q);
	}
      else if ((!g_strcasecmp(ext, "pgm")))
	{
	  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 1);
	  gtk_widget_hide(p);
	  gtk_widget_hide(q);
	}
      else if ((!g_strcasecmp(ext, "ps")))
	{
	  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 2);
	  gtk_widget_hide(q);
	  gtk_widget_show(p);
	}
      else if ((!g_strcasecmp(ext, "jpg")) ||
	       (!g_strcasecmp(ext, "jpeg")))
	{
	  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 3);
	  gtk_widget_hide(p);
	  gtk_widget_show(q);
	}
      else if ((!g_strcasecmp(ext, "png")))
	{
	  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 4);
	  gtk_widget_hide(p);
	  gtk_widget_hide(q);
	}
      else if ((!g_strcasecmp(ext, "tif")) ||
	       (!g_strcasecmp(ext, "tiff")))
	{
	  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 5);
	  gtk_widget_hide(p);
	  gtk_widget_hide(q);
	}
      else
	{
	  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 6);
	  gtk_widget_hide(p);
	  gtk_widget_hide(q);
	}

      if (!preview)
	return;
      
      if ((isfile(file)) && (filesize(file) > 0))
	{

	  im = gdk_imlib_load_image(file);
	  if (im)
	    {
	      if ((im->rgb_width * 3) > (im->rgb_height * 4))
		{
		  ww = 240;
		  hh = (ww * im->rgb_height) / im->rgb_width;
		}
	      else
		{
		  hh = 180;
		  ww = (hh * im->rgb_width) / im->rgb_height;
		}
	      gtk_widget_set_usize(a, ww, hh);
	      gtk_widget_queue_resize(b);
	      gdk_imlib_render(im, ww, hh);
	      pmap = gdk_imlib_move_image(im);
	      mask = gdk_imlib_move_mask(im);
	      gdk_window_set_back_pixmap(a->window, pmap, FALSE);
	      gdk_window_clear(a->window);
	      gdk_window_shape_combine_mask(a->window, mask, 0, 0);
	      gdk_imlib_free_pixmap(pmap);
	    }
	  t = moddate(file);
	  file2 = ctime(&t);
	  gtk_clist_set_text(GTK_CLIST(c), 5, 1, file2);
	  i = filesize(file);
	  if (i > 0)
	    {
	      i = i >> 10;
	      /* If it's > 0, better show 1k than 0k */
	      if (i == 0)
		i = 1;
	    }
	  g_snprintf(tmp, sizeof(tmp), "%i kb", i);
	  gtk_clist_set_text(GTK_CLIST(c), 3, 1, tmp);
	}
      else
	{
	  gtk_clist_set_text(GTK_CLIST(c), 3, 1, "");
	  gtk_clist_set_text(GTK_CLIST(c), 5, 1, "");
	}
      file2 = fullfileof(file);
      gtk_clist_set_text(GTK_CLIST(c), 0, 1, file2);
      g_free(file2);
    }
  if (!preview)
    return;
  if (!im)
    {
      g_snprintf(tmp, sizeof(tmp), " ");
      gtk_clist_set_text(GTK_CLIST(c), 1, 1, tmp);
      g_snprintf(tmp, sizeof(tmp), " ");
      gtk_clist_set_text(GTK_CLIST(c), 2, 1, tmp);
      g_snprintf(tmp, sizeof(tmp), " ");
      gtk_clist_set_text(GTK_CLIST(c), 4, 1, tmp);
      gtk_widget_set_usize(a, 240, 180);
      gtk_widget_queue_resize(b);
      gdk_window_set_back_pixmap(a->window, NULL, TRUE);
      gdk_window_clear(a->window);
      gdk_window_shape_combine_mask(a->window, NULL, 0, 0);
    }
  else
    {
      g_snprintf(tmp, sizeof(tmp), "%s", extension(file));
      gtk_clist_set_text(GTK_CLIST(c), 1, 1, tmp);
      g_snprintf(tmp, sizeof(tmp), "%ix%i", im->rgb_width, im->rgb_height);
      gtk_clist_set_text(GTK_CLIST(c), 2, 1, tmp);
      if (im->shape_color.r >= 0)
	g_snprintf(tmp, sizeof(tmp), _("Yes"));
      else
	g_snprintf(tmp, sizeof(tmp), _("No"));
      gtk_clist_set_text(GTK_CLIST(c), 4, 1, tmp);
      gdk_imlib_destroy_image(im);
    }
  widget = NULL;
}

static void
ee_filesel_cb_destroy(GtkWidget * widget, gpointer * data)
{
  gtk_widget_hide(widget);
  data = NULL;
}

static void
ee_filesel_cb_cancel(GtkWidget * widget, gpointer * data)
{
  GtkWidget          *w;

  w = GTK_WIDGET(data);
  gtk_widget_hide(w);
  widget = NULL;
}

GtkWidget          *
ee_filesel_new(void)
{
  GtkWidget          *w, *f, *h, *b, *a, *v, *c, *mi, *m, *om, *hh, *l, *s, *e;
  GtkWidget          *hhh, *sw;
  GtkAdjustment      *adj, *adj2;
  GdkImlibSaveInfo   *si;

  if (!ok_clicked_id)
    ok_clicked_id =
      gtk_object_class_user_signal_new (gtk_type_class (gtk_window_get_type ()),
					"ok_clicked", 0,
					gtk_marshal_NONE__STRING,
					GTK_TYPE_NONE /* `void' return.*/ ,
					1, GTK_TYPE_STRING);

  w = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_position(GTK_WINDOW(w), GTK_WIN_POS_MOUSE);
  gtk_window_set_wmclass(GTK_WINDOW(w), "ee-filesel", _("Image Viewer"));
  gtk_window_set_title(GTK_WINDOW(w), _("Select a file to load"));
  gtk_container_border_width(GTK_CONTAINER(w), 2);
  gtk_window_set_policy(GTK_WINDOW(w), 0, 1, 1);
  gtk_signal_connect(GTK_OBJECT(w), "delete_event",
		     GTK_SIGNAL_FUNC(ee_filesel_cb_destroy), w);
  gtk_signal_connect(GTK_OBJECT(w), "destroy",
		     GTK_SIGNAL_FUNC(ee_filesel_cb_kill), w);

  gtk_object_set_data(GTK_OBJECT(w), "filename", NULL);
  gtk_object_set_data(GTK_OBJECT(w), "dosave", GINT_TO_POINTER (0));
  si = g_malloc(sizeof(GdkImlibSaveInfo));
  si->quality = (80 * 255) / 100;
  si->scaling = 1024;
  si->xjustification = 512;
  si->yjustification = 512;
  si->page_size = PAGE_SIZE_LETTER;
  si->color = 1;
  gtk_object_set_data(GTK_OBJECT(w), "save_info", si);

  v = gtk_vbox_new(FALSE, 2);
  gtk_widget_show(v);
  gtk_container_add(GTK_CONTAINER(w), v);

  h = gtk_hbox_new(FALSE, 2);
  gtk_widget_show(h);
  gtk_box_pack_start(GTK_BOX(v), h, TRUE, TRUE, 0);

  b = gtk_frame_new(_("Saved Image Settings"));
  gtk_box_pack_start(GTK_BOX(v), b, FALSE, FALSE, 0);
  gtk_object_set_data(GTK_OBJECT(w), "save", b);

  hhh = gtk_hbox_new(FALSE, 2);
  gtk_widget_show(hhh);
  gtk_box_pack_start(GTK_BOX(v), hhh, TRUE, TRUE, 0);
  l = gtk_label_new(_("Print Command:"));
  gtk_widget_show(l);
  gtk_box_pack_start(GTK_BOX(hhh), l, FALSE, FALSE, 0);
  e = gtk_entry_new();
  gtk_widget_show(e);
  gtk_box_pack_start(GTK_BOX(hhh),
		     e, TRUE, TRUE, 0);
  gtk_object_set_data(GTK_OBJECT(w), "actual_entry", e);
  gtk_object_set_data(GTK_OBJECT(w), "entry", hhh);
  
  hhh = gtk_hbutton_box_new();
  gtk_button_box_set_spacing(GTK_BUTTON_BOX(hhh), 4);
  gtk_box_pack_start(GTK_BOX(v), hhh, TRUE, TRUE, 0);
  c = gtk_button_new_with_label(_("Print"));
  gtk_widget_show(c);
  gtk_box_pack_start(GTK_BOX(hhh),
		     c, FALSE, FALSE, 0);
  gtk_signal_connect(GTK_OBJECT(c),
		     "clicked", GTK_SIGNAL_FUNC(ee_filesel_cb_ok), w);
  c = gtk_button_new_with_label(_("Cancel"));
  gtk_widget_show(c);
  gtk_box_pack_start(GTK_BOX(hhh),
		     c, FALSE, FALSE, 0);
  gtk_signal_connect(GTK_OBJECT(c),
		     "clicked", GTK_SIGNAL_FUNC(ee_filesel_cb_cancel), w);
  gtk_object_set_data(GTK_OBJECT(w), "print_button", hhh);
  
  hh = gtk_hbox_new(FALSE, 2);
  gtk_widget_show(hh);
  gtk_container_add(GTK_CONTAINER(b), hh);

  l = gtk_label_new(_("Image Format:"));
  gtk_widget_show(l);
  gtk_box_pack_start(GTK_BOX(hh), l, FALSE, FALSE, 0);
  gtk_object_set_data(GTK_OBJECT(w), "image_format", l);

  v = gtk_vbox_new(FALSE, 2);
  gtk_widget_show(v);
  gtk_box_pack_start(GTK_BOX(hh), v, FALSE, FALSE, 0);

  m = gtk_menu_new();
  gtk_widget_show(m);

  om = gtk_option_menu_new();
  gtk_widget_set_usize(om, 128, -1);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(om), m);
  gtk_widget_show(om);
  gtk_box_pack_start(GTK_BOX(v), om, TRUE, FALSE, 0);

  mi = gtk_menu_item_new_with_label("PPM (PNM)");
  gtk_object_set_data(GTK_OBJECT(mi), "extension", "ppm");
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_format),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label("PGM (PNM)");
  gtk_object_set_data(GTK_OBJECT(mi), "extension", "pgm");
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_format),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label(_("PS (PostScript)"));
  gtk_object_set_data(GTK_OBJECT(mi), "extension", "ps");
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_format),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label("JPEG");
  gtk_object_set_data(GTK_OBJECT(mi), "extension", "jpg");
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_format),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label("PNG");
  gtk_object_set_data(GTK_OBJECT(mi), "extension", "png");
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_format),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label("TIFF");
  gtk_object_set_data(GTK_OBJECT(mi), "extension", "tif");
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_format),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label(_("Other (Automatic by extension)"));
  gtk_object_set_data(GTK_OBJECT(mi), "extension", "???");
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_format),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);

  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 6);
  gtk_object_set_data(GTK_OBJECT(w), "optionmenu", om);

  b = gtk_frame_new(_("Quality Settings"));
  gtk_box_pack_start(GTK_BOX(hh), b, TRUE, TRUE, 0);
  gtk_object_set_data(GTK_OBJECT(w), "quality", b);
		      
  adj = GTK_ADJUSTMENT(gtk_adjustment_new(80.0, 0.0, 100.0, 1.0, 1.0, 1.0));
  v = gtk_hscale_new(adj);
  gtk_widget_show(v);
  gtk_container_add(GTK_CONTAINER(b), v);
  gtk_signal_connect(GTK_OBJECT(adj), "value_changed",
		     GTK_SIGNAL_FUNC(ee_filesel_cb_quality_change), w);
  gtk_object_set_data(GTK_OBJECT(w), "quality_adj", adj);

  b = gtk_frame_new(_("Page Settings"));
  gtk_box_pack_start(GTK_BOX(hh), b, TRUE, TRUE, 0);
  gtk_object_set_data(GTK_OBJECT(w), "page", b);

  v = gtk_vbox_new(FALSE, 2);
  gtk_widget_show(v);
  gtk_container_add(GTK_CONTAINER(b), v);

  hh = gtk_hbox_new(FALSE, 2);
  gtk_widget_show(hh);
  gtk_box_pack_start(GTK_BOX(v), hh, FALSE, FALSE, 0);

  m = gtk_menu_new();
  gtk_widget_show(m);

  om = gtk_option_menu_new();
  gtk_widget_set_usize(om, 128, -1);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(om), m);
  gtk_widget_show(om);
  gtk_box_pack_start(GTK_BOX(hh), om, TRUE, TRUE, 0);

  mi = gtk_menu_item_new_with_label("Executive");
  gtk_object_set_data(GTK_OBJECT(mi), "data", GINT_TO_POINTER (PAGE_SIZE_EXECUTIVE));
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_page),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label("Letter");
  gtk_object_set_data(GTK_OBJECT(mi), "data", GINT_TO_POINTER (PAGE_SIZE_LETTER));
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_page),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label("Legal");
  gtk_object_set_data(GTK_OBJECT(mi), "data", GINT_TO_POINTER (PAGE_SIZE_LEGAL));
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_page),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label("A4");
  gtk_object_set_data(GTK_OBJECT(mi), "data", GINT_TO_POINTER (PAGE_SIZE_A4));
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_page),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label("A3");
  gtk_object_set_data(GTK_OBJECT(mi), "data", GINT_TO_POINTER (PAGE_SIZE_A3));
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_page),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label("A5");
  gtk_object_set_data(GTK_OBJECT(mi), "data", GINT_TO_POINTER (PAGE_SIZE_A5));
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_page),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);
  mi = gtk_menu_item_new_with_label("Folio");
  gtk_object_set_data(GTK_OBJECT(mi), "data", GINT_TO_POINTER (PAGE_SIZE_FOLIO));
  gtk_signal_connect(GTK_OBJECT(mi),
		     "activate", GTK_SIGNAL_FUNC(ee_filesel_cb_set_page),
		     w);
  gtk_menu_append(GTK_MENU(m), mi);
  gtk_widget_show(mi);

  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 1);

  c = gtk_check_button_new_with_label(_("Color"));
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(c), 1);
  gtk_widget_show(c);
  gtk_box_pack_start(GTK_BOX(hh),
		     c, FALSE, FALSE, 0);
  gtk_signal_connect(GTK_OBJECT(c), "clicked",
		     GTK_SIGNAL_FUNC(ee_filesel_cb_color_toggle),
		     w);

  hh = gtk_hbox_new(FALSE, 2);
  gtk_widget_show(hh);
  gtk_box_pack_start(GTK_BOX(v), hh, FALSE, FALSE, 0);

  l = gtk_label_new(_("Scaling Factor:"));
  gtk_widget_show(l);
  gtk_box_pack_start(GTK_BOX(hh), l, FALSE, FALSE, 0);

  adj = GTK_ADJUSTMENT(gtk_adjustment_new(0, 1, 101, 1, 1, 1));
  a = gtk_hscale_new(adj);
  gtk_scale_set_digits(GTK_SCALE(a), 0);
  gtk_widget_show(a);
  gtk_box_pack_start(GTK_BOX(hh),
		     a, TRUE, TRUE, 0);
  gtk_signal_connect(GTK_OBJECT(adj), "value_changed",
		     GTK_SIGNAL_FUNC(ee_filesel_cb_scale_change), w);
  gtk_object_set_data(GTK_OBJECT(w), "scale_adj", adj);


  adj = GTK_ADJUSTMENT(gtk_adjustment_new(0.5, 0.0, 1.0, 1.0, 1.0, 1.0));
  adj2 = GTK_ADJUSTMENT(gtk_adjustment_new(0.5, 0.0, 1.0, 1.0, 1.0, 1.0));

  s = gtk_scrollpane_new(adj, adj2, 1.0);
  gtk_box_pack_start(GTK_BOX(hh), s, TRUE, FALSE, 0);
  gtk_widget_show(s);
  gtk_widget_set_usize(s, 60, 80);
  gtk_object_set_data(GTK_OBJECT(w), "page_adj", adj);
  gtk_object_set_data(GTK_OBJECT(w), "page_adj2", adj2);
  gtk_signal_connect(GTK_OBJECT(adj), "value_changed",
		     GTK_SIGNAL_FUNC(ee_filesel_cb_page_adj_change), w);
  gtk_signal_connect(GTK_OBJECT(adj2), "value_changed",
		     GTK_SIGNAL_FUNC(ee_filesel_cb_page_adj_change), w);
  
  f = gtk_file_selection_new(_("Select a file"));
  gtk_file_selection_show_fileop_buttons(GTK_FILE_SELECTION(f));
  gtk_widget_reparent(GTK_FILE_SELECTION(f)->main_vbox, h);
  gtk_widget_show(GTK_FILE_SELECTION(f)->main_vbox);
  gtk_clist_set_selection_mode(GTK_CLIST(GTK_FILE_SELECTION(f)->file_list), 
			       GTK_SELECTION_BROWSE);
  gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(f)->ok_button),
		     "clicked", GTK_SIGNAL_FUNC(ee_filesel_cb_ok), w);
  gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(f)->cancel_button),
		     "clicked", GTK_SIGNAL_FUNC(ee_filesel_cb_cancel), w);

  c = gtk_button_new_with_label(_("Load all files in directory"));
  gtk_widget_show(c);
  gtk_box_pack_start(GTK_BOX(GTK_FILE_SELECTION(f)->main_vbox),
		     c, FALSE, FALSE, 0);
  gtk_signal_connect(GTK_OBJECT(c),
		     "clicked", GTK_SIGNAL_FUNC(ee_filesel_cb_loadall), w);
  gtk_object_set_data(GTK_OBJECT(w), "load_all", c);
  
  gtk_object_set_data(GTK_OBJECT(w), "filesel", f);
  gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(f)->selection_entry),
		     "changed", GTK_SIGNAL_FUNC(ee_filesel_cb_select),
		     w);

  gtk_object_set_data(GTK_OBJECT(w), "preview", GINT_TO_POINTER (1));
  c = gtk_check_button_new_with_label(_("Use Previews"));
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(c), 1);
  gtk_widget_show(c);
  gtk_box_pack_start(GTK_BOX(GTK_FILE_SELECTION(f)->main_vbox),
		     c, FALSE, TRUE, 0);
  gtk_signal_connect(GTK_OBJECT(c), "clicked",
		     GTK_SIGNAL_FUNC(ee_filesel_cb_toggle),
		     w);
  gtk_object_set_data(GTK_OBJECT(w), "load_all", c);

  b = gtk_frame_new(_("Image Information"));
  gtk_widget_show(b);
  gtk_box_pack_start(GTK_BOX(h), b, FALSE, FALSE, 0);
  gtk_object_set_data(GTK_OBJECT(w), "outerframe", b);

  v = gtk_vbox_new(FALSE, 2);
  gtk_widget_show(v);
  gtk_container_add(GTK_CONTAINER(b), v);

  a = gtk_alignment_new(0.5, 0.5, 0.0, 0.0);
  gtk_widget_show(a);
  gtk_box_pack_start(GTK_BOX(v), a, FALSE, FALSE, 0);

  b = gtk_frame_new(NULL);
  gtk_widget_show(b);
  gtk_container_add(GTK_CONTAINER(a), b);

  gtk_object_set_data(GTK_OBJECT(w), "frame", b);

  a = gtk_drawing_area_new();
  gtk_widget_set_usize(a, 240, 180);
  gtk_widget_show(a);
  gtk_container_add(GTK_CONTAINER(b), a);
  gtk_widget_realize(a);
  gdk_window_set_back_pixmap(a->window, NULL, TRUE);

  gtk_object_set_data(GTK_OBJECT(w), "mini", a);

  sw = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_show(sw);
  c = gtk_clist_new(2);
  gtk_widget_show(c);
  gtk_container_add(GTK_CONTAINER(sw), c);
  gtk_box_pack_start(GTK_BOX(v), sw, TRUE, TRUE, 0);

  gtk_widget_set_usize(c, 248, -1);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_clist_set_selection_mode(GTK_CLIST(c), GTK_SELECTION_BROWSE);
  gtk_clist_set_column_justification(GTK_CLIST(c), 0, GTK_JUSTIFY_RIGHT);
  gtk_clist_set_column_justification(GTK_CLIST(c), 1, GTK_JUSTIFY_LEFT);
  gtk_clist_set_column_width(GTK_CLIST(c), 0, 110);
  gtk_clist_set_column_width(GTK_CLIST(c), 1, 110);
  gtk_object_set_data(GTK_OBJECT(w), "info", c);
  {
    gchar              *txt[] = {N_("Filename:"), ""};

    txt[0]=_(txt[0]);
    gtk_clist_insert(GTK_CLIST(c), 0, txt);
  }
  {
    gchar              *txt[] = {N_("Format:"), ""};

    txt[0]=_(txt[0]);
    gtk_clist_insert(GTK_CLIST(c), 1, txt);
  }
  {
    gchar              *txt[] = {N_("Dimensions:"), ""};

    txt[0]=_(txt[0]);
    gtk_clist_insert(GTK_CLIST(c), 2, txt);
  }
  {
    gchar              *txt[] = {N_("File Size:"), ""};

    txt[0]=_(txt[0]);
    gtk_clist_insert(GTK_CLIST(c), 3, txt);
  }
  {
    gchar              *txt[] = {N_("Has Transparency:"), ""};

    txt[0]=_(txt[0]);
    gtk_clist_insert(GTK_CLIST(c), 4, txt);
  }
  {
    gchar              *txt[] = {N_("Last Modified Date:"), ""};

    txt[0]=_(txt[0]);
    gtk_clist_insert(GTK_CLIST(c), 5, txt);
  }

  return w;
}
