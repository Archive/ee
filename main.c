/*****************             Electric Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/
#include "ee.h"

#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

static gint        set_root_and_quit = 0;
static gchar       *current_dir;
static gchar      **start_img_list = NULL;
static gint         start_img_list_num = 0;

extern gint toolbar_pos;

struct poptOption options[] = {
	{"root", 'r', POPT_ARG_NONE, &set_root_and_quit, 1,
	 N_("Set root window image and leave"), NULL},
	{NULL, '\0', 0, NULL, 0}
};

enum {
  TARGET_URI_LIST,
  TARGET_URL
};

static void 
load_file(gchar *name)
{
  GdkImlibImage *im;

  im = gdk_imlib_load_image(name);
  if (im) 
    {
      ee_image_set_filename(image_display, name);
      ee_image_set_image(image_display, im);
      ee_list_add_filename(image_list, name); 
    }
}

/* The following functions are a quick hack to get URL drops
 * from Netscape working. I make no pretense of correctness
 * or great security. Plus it leaves files lying in temp.
 * but it's fun.
 */

typedef struct {
  gchar *name;
  gint pid;
} ChildData;

/* Called when the child quits */
static gboolean
input_func(GIOChannel *source, GIOCondition condition, gpointer data)
{
  char buf[2];
  ChildData *child_data = data;
  int status;

  if (read (g_io_channel_unix_get_fd(source), buf, 2) != 2 ||
      (buf[0] != 'O') || (buf[1] != 'K')) /* failure */
    {
      g_warning ("Download failed!\n");
    }
  else
    {
      load_file (child_data->name);
    }

  waitpid (child_data->pid, &status, 0);

  g_free (child_data->name);
  g_free (child_data);
  
  close(g_io_channel_unix_get_fd(source));

  return FALSE;
}

static char *download_dir = NULL;

static void 
grab_url(gchar *name)
{
  int pid;
  int fds[2];

  if (!download_dir)
    {
      char buffer[L_tmpnam];
  
      if (!tmpnam(buffer) || (mkdir(buffer, 0755) != 0))
	{
	  g_warning ("Could not generate temporary directory: %s\n",
		     g_strerror(errno));
	  return;
	}
      
      download_dir = g_strdup (buffer);
    }

  if (pipe(fds))
    {
      g_warning ("Could not create pipe: %s\n", g_strerror(errno));
      return;
    }
  
  if (!(pid = fork()))
    {
      /* Child */

      close(fds[0]);
      
      /* Fork off a wget */

      if (!(pid = fork()))
	{
	  execlp("wget", "wget", "-q", "-P", download_dir, name, NULL);
	  g_warning("Could not run wget: %s\n", g_strerror(errno));
	  _exit(0);
	}
      else if (pid > 0)
	{
	  int status;
	  waitpid (pid, &status, 0);

	  if (!status)
	    write (fds[1], "OK", 2);

	  _exit(0);
	}
      else
	{
	  g_warning ("Could not fork!\n");
	  _exit(0);
	}
    }
  else if (pid > 0)
    {
      /* Parent */

      char *tail;
      ChildData *child_data;
      GIOChannel *ioc;

      close(fds[1]);

      tail = strrchr(name, '/');
      if (tail)
	tail++;
      else
	tail = name;
      
      child_data = g_new0 (ChildData, 1);
      child_data->name = g_strconcat (download_dir,"/",tail,NULL);
      child_data->pid = pid;

      ioc = g_io_channel_unix_new (fds[0]);
      g_io_add_watch(ioc, G_IO_IN|G_IO_HUP|G_IO_NVAL, input_func, child_data);
      g_io_channel_unref(ioc);
    }
  else
    g_warning ("Could not fork\n");
}

static void
process_one_filename (char *filename)
{
	if (strncmp (filename, "file:", 5) == 0)
		load_file (filename + 5);
	else
		grab_url (filename);
}

static void
filenames_dropped(GtkWidget * widget,
		  GdkDragContext   *context,
		  gint              x,
		  gint              y,
		  GtkSelectionData *selection_data,
		  guint             info,
		  guint             time)
{
	GList *names;
	char *filename = NULL;
	
	if (!selection_data->data)
		return;
	
	switch (info){
	case TARGET_URI_LIST:
		names = gnome_uri_list_extract_uris ((char *)selection_data->data);
		
		if (!names)
			return;
		
		for (; names; names = names->next){
			filename = names->data;

			process_one_filename (filename);
		}
		gnome_uri_list_free_strings (names);
		break;
		
	case TARGET_URL:
		process_one_filename ((char *)selection_data->data);
		break;
	}
}

void
configure_drop_on_widget(GtkWidget * widget)
{
  static GtkTargetEntry drag_types[] =
  {
    { "text/uri-list", 0, TARGET_URI_LIST },
    { "_NETSCAPE_URL", 0, TARGET_URL }
  };
  static gint n_drag_types = sizeof(drag_types)/sizeof(drag_types[0]);

  gtk_drag_dest_set (widget,
		     GTK_DEST_DEFAULT_MOTION |
		     GTK_DEST_DEFAULT_HIGHLIGHT |
		     GTK_DEST_DEFAULT_DROP,
		     drag_types, n_drag_types,
		     GDK_ACTION_COPY);

  gtk_signal_connect(GTK_OBJECT(widget), "drag_data_received",
		       GTK_SIGNAL_FUNC(filenames_dropped), NULL);
}

static void
drag_data_get (GtkWidget        *widget,
	       GdkDragContext   *context,
	       GtkSelectionData *selection_data,
	       guint             info,
	       guint             time)
{
  gchar *file;
  gchar *uri_list;

  file = ee_image_get_filename(image_display);

  if (file) /* ignore non-file-images for now */
    {
      uri_list = g_strconcat ("file:", file, NULL);
      gtk_selection_data_set (selection_data,
			      selection_data->target, 8,
			      (void *)uri_list, strlen((char *)uri_list));
      g_free (uri_list);
    }
  else
    {
      gtk_selection_data_set (selection_data,
			      selection_data->target, 8,
			      NULL, 0);
    }
}

void
configure_drag_on_widget(GtkWidget * widget)
{
  static GtkTargetEntry drag_types[] =
  {
    { "text/uri-list", 0, TARGET_URI_LIST }
  };
  static gint n_drag_types = sizeof(drag_types)/sizeof(drag_types[0]);

  gtk_drag_source_set (widget, 
		       GDK_BUTTON1_MASK,
		       drag_types, n_drag_types,
		       GDK_ACTION_COPY);

  gtk_signal_connect(GTK_OBJECT(widget), "drag_data_get",
		     GTK_SIGNAL_FUNC(drag_data_get), NULL);
}

int
main(int argc, char **argv)
{
  gint i;
  GdkImlibImage *im;
  GtkWidget *view;
  poptContext ctx;
  const char **leftovers;
  
  bindtextdomain(PACKAGE, GNOMELOCALEDIR);
  textdomain(PACKAGE);

  current_dir = cwd();

  gnome_init_with_popt_table("ee", VERSION, argc, argv, options, 0, &ctx);

  leftovers = poptGetArgs(ctx);
  start_img_list_num = 0;

  if(leftovers) {
    for(; leftovers[start_img_list_num];
	start_img_list_num++) {

      start_img_list = (gchar **)
	g_realloc(start_img_list, (start_img_list_num+2)* sizeof(gchar *));

      if (*leftovers[start_img_list_num] != '/')
	start_img_list[start_img_list_num] = g_strconcat(current_dir, "/",
							    leftovers[start_img_list_num], NULL);
      else
        start_img_list[start_img_list_num] = g_strdup(leftovers[start_img_list_num]);
    }
    start_img_list[start_img_list_num] = NULL;
  }

  poptFreeContext(ctx);

  gdk_imlib_init();
  gtk_widget_push_visual(gdk_imlib_get_visual());
  gtk_widget_push_colormap(gdk_imlib_get_colormap());
  ee_conf_load();

  if (start_img_list_num < 1)
    toolbar_pos++;
  
  ee_thumb_init_dirs();
  main_menu = ee_menu_new();
  image_display = ee_image_new();  
  image_list = ee_list_new();
  ee_list_freeze(image_list);
  if (start_img_list_num > 0)
    {
      im = gdk_imlib_load_image(start_img_list[0]);
      ee_image_set_image(image_display, im);
      ee_image_set_filename(image_display, g_strdup(start_img_list[0]));
       for (i = 0; i < start_img_list_num; i++)
        {
          ee_list_add_filename(image_list, start_img_list[i]);
          g_free(start_img_list[i]);
        }
      g_free(start_img_list);
    }
  else
    ee_image_set_image(image_display, NULL);
  ee_list_thaw(image_list);
  ee_image_set_popup_menu(image_display, main_menu);
  
  while (gtk_events_pending())
    gtk_main_iteration();

  if (start_img_list_num > 1)
    gtk_widget_show(image_list);
  gtk_widget_show(image_display);
  ee_conf_set_options();
  while (gtk_events_pending())
    gtk_main_iteration();
  ee_conf_set_options();

  configure_drop_on_widget(image_list);
  configure_drop_on_widget(image_display);
  view = gtk_object_get_data (GTK_OBJECT (image_display), "view");
  configure_drag_on_widget(view);

  gtk_main();
  return 0;
}
