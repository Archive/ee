/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#ifndef __EE_THUMB_H__
#define __EE_THUMB_H__

#include <gtk/gtk.h>
#include <gdk_imlib.h>
#include <gnome.h>

#ifdef __cplusplus
extern              "C"
{
#endif                          /* __cplusplus */
void           ee_thumb_init_dirs(void);
GdkImlibImage *ee_thumb_find(gchar *file, gchar *size, gint maxw, gint maxh);
#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif
