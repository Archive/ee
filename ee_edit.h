/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#ifndef __EE_EDIT_H__
#define __EE_EDIT_H__

#include "config.h"
#include <gtk/gtk.h>
#include <gdk_imlib.h>
#include <gnome.h>

#ifdef __cplusplus
extern              "C"
{
#endif                          /* __cplusplus */
  GtkWidget          *ee_edit_new(void);
  void ee_edit_update_graphs(GtkWidget *w);
  void ee_edit_update_preview(GtkWidget *widget);
  void ee_edit_set_crop_label(GtkWidget *widget, gchar *txt);
  void ee_edit_set_size_label(GtkWidget *widget, gchar *txt);
#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif
