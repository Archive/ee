/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#include <ee_toolbar.h>
#include "functions.h"
#include <stdio.h>
#include <stdlib.h>

/* major hack */
#include "gnome-stock-menu-first.xpm"
#include "gnome-stock-menu-last.xpm"

GtkWidget          *
ee_toolbar_new(void)
{
  GtkWidget *w, *icon;

  w = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_ICONS);
  
  icon = gnome_stock_pixmap_widget(w, GNOME_STOCK_MENU_QUIT);
  gtk_toolbar_append_item(GTK_TOOLBAR(w), 
			  _("Exit"), 
			  _("Exit Electric Eyes"),
			  _("Exit Electric Eyes"),
			  icon,
			  func_exit,
			  NULL);
  icon = gnome_stock_pixmap_widget(w, GNOME_STOCK_MENU_OPEN);
  gtk_toolbar_append_item(GTK_TOOLBAR(w), 
			  _("Open"), 
			  _("Open a new file"),
			  _("Open a new file"),
			  icon,
			  func_open_image,
			  NULL);
  icon = gnome_stock_pixmap_widget(w, GNOME_STOCK_MENU_SAVE);
  gtk_toolbar_append_item(GTK_TOOLBAR(w), 
			  _("Save"), 
			  _("Save as original filename"),
			  _("Save as original filename"),
			  icon,
			  func_save_image_only,
			  NULL);
  icon = gnome_stock_pixmap_widget(w, GNOME_STOCK_MENU_SAVE_AS);
  gtk_toolbar_append_item(GTK_TOOLBAR(w), 
			  _("Save As"), 
			  _("Save as new file"),
			  _("Save as new file"),
			  icon,
			  func_save_image,
			  NULL);
  icon = gnome_stock_pixmap_widget(w, GNOME_STOCK_MENU_PRINT);
  gtk_toolbar_append_item(GTK_TOOLBAR(w), 
			  _("Print"), 
			  _("Print image"),
			  _("Print image"),
			  icon,
			  func_print_image,
			  NULL);
  icon = gnome_pixmap_new_from_xpm_d(gnome_stock_menu_first_xpm);
  gtk_toolbar_append_item(GTK_TOOLBAR(w), 
			  _("First"), 
			  _("View the first image in the list"),
			  _("View the first image in the list"),
			  icon,
			  func_first,
			  NULL);
  icon = gnome_stock_pixmap_widget(w, GNOME_STOCK_MENU_BACK);
  gtk_toolbar_append_item(GTK_TOOLBAR(w), 
			  _("Previous"), 
			  _("View the previous image in the list"),
			  _("View the previous image in the list"),
			  icon,
			  func_prev,
			  NULL);
  icon = gnome_stock_pixmap_widget(w, GNOME_STOCK_MENU_FORWARD);
  gtk_toolbar_append_item(GTK_TOOLBAR(w), 
			  _("Next"), 
			  _("View the next image in the list"),
			  _("View the next image in the list"),
			  icon,
			  func_next,
			  NULL);
  icon = gnome_pixmap_new_from_xpm_d(gnome_stock_menu_last_xpm);
  gtk_toolbar_append_item(GTK_TOOLBAR(w), 
			  _("Last"), 
			  _("View the last image in the list"),
			  _("View the last image in the list"),
			  icon,
			  func_last,
			  NULL);
  icon = gnome_stock_pixmap_widget(w, GNOME_STOCK_MENU_CUT);
  gtk_toolbar_append_item(GTK_TOOLBAR(w), 
			  _("Crop"), 
			  _("Crop the current selection"),
			  _("Crop the current selection"),
			  icon,
			  func_crop_image,
			  NULL);
  return w;
}

