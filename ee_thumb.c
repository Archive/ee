/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#include <ee_thumb.h>
#include <ee_file.h>
#include <stdio.h>
#include <stdlib.h>
#include "globals.h"

void
ee_thumb_init_dirs(void)
{
  gchar               *hom;
  gchar               *s;
  
  hom = homedir(getuid());
  if (!hom)
    return;
  s = g_strconcat(hom, "/.ee/icons/", NULL);
  if (!exists(s))
    mkdirs(s);
  g_free(s);
  s = g_strconcat(hom, "/.ee/minis/", NULL);
  if (!exists(s))
    mkdirs(s);
  g_free(s);
  g_free(hom);
}

GdkImlibImage *
ee_thumb_find(gchar *file, gchar *size, gint maxw, gint maxh)
{
  gchar               *hom;
  gchar               *s;
  gint                 w, h;
  GdkImlibImage       *im, *im2;  
  
  hom = homedir(getuid());
  if (!hom)
    return NULL;    
    
  s = g_strconcat(hom, "/.ee/", size, "/", file, NULL);
  if (isfile(s))
    {
      im = gdk_imlib_load_image(s);
      g_free(s);
      if (im)
	{
	  g_free(hom);
	  return im;
	}
    }
  else
    g_free(s);
  im = gdk_imlib_load_image(file);
  if (!im)
    {
      g_free(hom);
      return NULL;
    }
  h = maxh;
  w = (im->rgb_width * h) / im->rgb_height;
  if (w > maxw)
    {
      w = maxw;
      h = (im->rgb_height * w) / im->rgb_width;
    }
  h = MAX(h, 1);
  w = MAX(w, 1);
  im2 = gdk_imlib_clone_scaled_image(im, w, h);
  gdk_imlib_destroy_image(im);
  s = g_strconcat(hom, "/.ee/", size, "/", file, NULL);
  mkdirs(s);
  gdk_imlib_save_image_to_ppm(im2, s);
  g_free(s);
  g_free(hom);
  return im2;
}
