/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#include <ee_conf.h>
#include <ee_file.h>
#include <ee_list.h>
#include <ee_image.h>
#include <stdio.h>
#include <stdlib.h>
#include "globals.h"
#include "functions.h"

gint large_thumbs = 0;
gint gen_thumbs = 1;
gint always_apply = 0;
gint scrolled_view = 0;
gint toolbar_pos = 0;

void 
ee_conf_save(void)
{
  gchar               *hom;
  gchar               *s;
  FILE                *f;
  
  hom = homedir(getuid());
  if (!hom)
    return;
  s = g_strconcat(hom, "/.ee/settings", NULL);
  g_free(hom);
  f = fopen(s, "w");
  g_free(s);
  scrolled_view = GPOINTER_TO_INT
    (gtk_object_get_data(GTK_OBJECT(image_display), "forcescroll"));
  toolbar_pos = GPOINTER_TO_INT
    (gtk_object_get_data(GTK_OBJECT(image_display), "toolbar_pos"));
  if (image_list)
    {
      gen_thumbs = GPOINTER_TO_INT
	(gtk_object_get_data(GTK_OBJECT(image_list), "do_icons"));
      large_thumbs = GPOINTER_TO_INT
	(gtk_object_get_data(GTK_OBJECT(image_list), "large"));
    }
  if (edit_window)
    {
      always_apply = GPOINTER_TO_INT
	(gtk_object_get_data(GTK_OBJECT(edit_window), "always"));
    }
  fprintf(f, "%i %i %i %i %i\n", 
	  large_thumbs, 
	  gen_thumbs, 
	  always_apply, 
	  scrolled_view, 
	  toolbar_pos);
  fclose(f);
}

void 
ee_conf_load(void)
{
  gchar               *hom;
  gchar               *s;
  FILE                *f;
  
  hom = homedir(getuid());
  if (!hom)
    return;
  s = g_strconcat(hom, "/.ee/settings", NULL);
  g_free(hom);
  f = fopen(s, "r");
  g_free(s);
  if (!f)
    return;
  fscanf(f, "%i %i %i %i %i\n", 
	  &large_thumbs, 
	  &gen_thumbs, 
	  &always_apply, 
	  &scrolled_view, 
	  &toolbar_pos);
  fclose(f);
}

void 
ee_conf_set_options(void)
{
  if (image_list)
    {
      if (gen_thumbs)
	ee_list_set_gen_thumbs(image_list);
      else
	ee_list_unset_gen_thumbs(image_list);
      if (large_thumbs)
	ee_list_set_large_thumbs(image_list);
      else
	ee_list_unset_large_thumbs(image_list);
    }
  if (edit_window)
    {
    }
  if (image_display)
    {
      if (toolbar_pos > 0)
	{
	  func_hide_toolbar(NULL, NULL);
	  switch (toolbar_pos)
	    {
	     case 1:
	      func_add_toolbar_top(NULL, NULL);
	      break;
	     case 2:
	      func_add_toolbar_bottom(NULL, NULL);
	      break;
	     case 3:
	      func_add_toolbar_left(NULL, NULL);
	      break;
	     case 4:
	      func_add_toolbar_right(NULL, NULL);
	      break;
	     default:
	      break;
	    }
	}
      ee_image_force_scrollable(image_display, scrolled_view);
    }
}

