/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

extern GtkWidget *file_selector, *image_display, *main_menu, *image_toolbar,
                 *padder_button, *image_list, *edit_window, *ee_app;

