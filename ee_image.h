/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#ifndef __EE_IMAGE_H__
#define __EE_IMAGE_H__

#include "config.h"
#include <gtk/gtk.h>
#include <gdk_imlib.h>

#ifdef __cplusplus
extern              "C"
{
#endif                          /* __cplusplus */
GtkWidget          *ee_image_new(void);
GdkImlibImage      *ee_image_get_image(GtkWidget *widget);
void                ee_image_add_toolbar(GtkWidget *widget, GtkWidget *toolbar, gint place);
void                ee_image_get_size(GtkWidget *widget, gint *width, gint *height);
void                ee_image_set_size(GtkWidget *widget, gint width, gint height);
void                ee_image_set_image_size(GtkWidget *widget, gint width, gint height);
void                ee_image_reset_size(GtkWidget *widget);
void                ee_image_set_image(GtkWidget *widget, GdkImlibImage *im);
void                ee_image_set_popup_menu(GtkWidget *widget, GtkWidget *menu);
void                ee_image_set_filename(GtkWidget *widget, gchar *file);
gchar              *ee_image_get_filename(GtkWidget *widget);
void                ee_image_force_scrollable(GtkWidget *widget, gint tru);
void                ee_image_do_crop(GtkWidget *widget);
void                ee_image_remove_crop(GtkWidget *widget);
      
#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif
