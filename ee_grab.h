/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#ifndef __EE_GRAB_H__
#define __EE_GRAB_H__

#include "config.h"
#include <gtk/gtk.h>
#include <gdk_imlib.h>
#include <gnome.h>

#ifdef __cplusplus
extern              "C"
{
#endif                          /* __cplusplus */
GtkWidget          *ee_grab_new(void);
void                ee_grab_select_window(void);
      
#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif
