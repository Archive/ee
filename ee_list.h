/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#ifndef __EE_LIST_H__
#define __EE_LIST_H__

#include "config.h"
#include <gtk/gtk.h>
#include <gdk_imlib.h>
#include <gnome.h>

#ifdef __cplusplus
extern              "C"
{
#endif                          /* __cplusplus */
  GtkWidget *ee_list_new(void);
  void ee_list_next(GtkWidget *widget, gpointer data);
  void ee_list_prev(GtkWidget *widget, gpointer data);
  void ee_list_start(GtkWidget *widget, gpointer data);
  void ee_list_end(GtkWidget *widget, gpointer data);
  void ee_list_sort(GtkWidget *widget, gpointer data);
  void ee_list_add_filename(GtkWidget *w, gchar *file);
  void ee_list_del_filename(GtkWidget *w, gchar *file);
  void ee_list_show_filename(GtkWidget *w, gchar *file);
  void ee_list_freeze(GtkWidget *w);
  void ee_list_thaw(GtkWidget *w);
  void ee_list_del_file(GtkWidget *w, gchar *file);
  void ee_list_set_gen_thumbs(GtkWidget *w);
  void ee_list_unset_gen_thumbs(GtkWidget *w);
  void ee_list_set_large_thumbs(GtkWidget *w);
  void ee_list_unset_large_thumbs(GtkWidget *w);     
#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif
